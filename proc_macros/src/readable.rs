use proc_macro2::{Ident, TokenStream};
use quote::{quote, quote_spanned};
use syn::{spanned::Spanned, Data, DataEnum, DataStruct, DeriveInput, Field, Fields, Meta};

pub fn hkxbinaryreadable(input: DeriveInput) -> TokenStream {
    let identificator = &input.ident;

    let implementation = match &input.data {
        Data::Struct(st) => impl_for_struct(st, identificator, &input),
        Data::Enum(en) => impl_for_enum(en, identificator),
        Data::Union(un) => quote_spanned! {
            un.union_token.span() =>
            compile_error!("cannot use on unions!")
        },
    };

    quote! {
        #[automatically_derived]
        impl crate::binary::traits::HkxBinaryReadable for #identificator {
            fn binary_read(br: &mut crate::binary::reader::HkxBinaryReader) -> Self {
                #implementation
            }
        }
    }
}

fn impl_for_struct(st: &DataStruct, identificator: &Ident, input: &DeriveInput) -> TokenStream {
    match &st.fields {
        Fields::Named(fields) => {
            let declarations = fields.named.iter().map(|field| {
                let field_name = &field.ident;
                let field_type = &field.ty;

                let current = quote! {
                    let pos_before_parsing_field = br.position();
                    let #field_name: #field_type = br.read();
                };

                let mut before: Vec<TokenStream> = vec![];
                let mut after: Vec<TokenStream> = vec![];

                handle_field_attributes(field, identificator, &mut before, &mut after);

                quote! {
                    #(#before)*
                    #current
                    #(#after)*
                }
            });

            let body = fields.named.iter().map(|f| {
                let field_name = &f.ident;
                quote! {#field_name,}
            });

            let align: Vec<_> = input
                .attrs
                .iter()
                .map(|a| {
                    if let Meta::List(li) = a.parse_meta().unwrap() {
                        let items = li.nested;
                        if li.path.get_ident().unwrap().to_string() == "pad" {
                            return quote! {
                                br.align(#items);
                            };
                        }
                        return quote!();
                    }
                    return quote!();
                })
                .collect();

            quote! {
                let pos_before_parsing_struct = br.position();
                #(#declarations)*
                let ret = Self {
                    #(#body)*
                };
                #(#align)*;
                ret
            }
        }
        Fields::Unnamed(fields) => {
            quote_spanned! {
                fields.span() =>
                compile_error!("cannot use on unnamed field structs!")
            }
        }
        Fields::Unit => {
            quote_spanned! {
                identificator.span() =>
                compile_error!("cannot use on unit structs!")
            }
        }
    }
}

fn handle_field_attributes(
    field: &Field,
    identificator: &Ident,
    before: &mut Vec<TokenStream>,
    after: &mut Vec<TokenStream>,
) {
    let identificator_string = identificator.to_string();

    let field_name = field.ident.as_ref().unwrap();
    let field_string = field_name.to_string();

    field.attrs.iter().for_each(|attr| {
        match attr.parse_meta().unwrap() {
            Meta::Path(path) => {
                let name = path.get_ident().unwrap().to_string();

                match &*name {
                    "info" => {
                        before.push(quote!{
                            let position = br.position();
                        });
                        after.push(quote!{
                            panic!("field_name: {}, position: {:#x}, value: {:?}", #field_string, position, #field_name);
                        });
                    }
                    "pad" => {
                        before.push(quote!{
                            if br.des.layout_rules.padding_option == 1 {
                                br.align(br.des.layout_rules.pointer_size.try_into().unwrap())
                            }
                        })
                    }
                    _ => ()
                }
            }
            Meta::List(li) => {
                let name = li.path.get_ident().unwrap().to_string();
                let items = li.nested;

                match &*name {
                    "assert" => {
                        let items = if items.len() == 1 {
                            quote!(#items)
                        } else {
                            quote!([#items])
                        };

                        after.push(quote!{
                            assert_eq!(#field_name, #items,
                                "\npos before struct: {:08x}\npos before field:  {:08x}\nstruct name: {:?}\nfield name: {}\n",
                                pos_before_parsing_struct,
                                pos_before_parsing_field,
                                #identificator_string,
                                #field_string,
                            );
                        })
                    }
                    "pad" => before.push(quote! {
                        br.align(#items);
                    }),
                    _ => (),
                }
            }
            _ => before.push(quote_spanned! {
                attr.span() =>
                compile_error!("invalid attribute!")
            }),
        }
    });
}

fn impl_for_enum(en: &DataEnum, identificator: &Ident) -> TokenStream {
    let variants: Vec<TokenStream> = en
        .variants
        .iter()
        .map(|v| {
            let ident = &v.ident;
            let name = ident.to_string();

            quote! {
                #name => Self::#ident(::std::rc::Rc::new(::std::cell::RefCell::new(br.read()))),
            }
        })
        .collect();

    let enum_name = identificator.to_string();
    quote! {
        let pos: u32 = br.position().try_into().unwrap();

        let src = pos - br.des.data_offset;
        let fixup = br.des.virtual_fixups.get(&src).expect(&*format!("cannot find fixup: {:x} ({:x})", src, pos));
        let classname = br.des.classnames.get(&fixup.dst).expect("cannot find classname");

        match &*classname.name {
            #(#variants)*
            _ => panic!("class '{}' doesn't belong inside enum '{}'", classname.name, #enum_name)
        }
    }
}
