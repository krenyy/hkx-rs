use proc_macro2::TokenStream;
use quote::quote;
use syn::{DeriveInput, Meta};

pub fn classinfo(input: DeriveInput) -> TokenStream {
    let identificator = input.ident;
    let name = identificator.to_string();

    let attrs = input
        .attrs
        .iter()
        .map(|a| a.parse_meta().unwrap())
        .collect::<Vec<_>>();

    let signature = match attrs.iter().find(|a| match a {
        Meta::NameValue(nv) => {
            if nv.path.get_ident().unwrap().to_string() == "signature" {
                return true;
            }
            false
        }
        _ => false,
    }) {
        Some(x) => match x {
            Meta::NameValue(nv) => {
                let lit = &nv.lit;
                quote!(#lit)
            }
            _ => unimplemented!(),
        },
        None => quote!(0),
    };

    quote! {
        #[automatically_derived]
        impl crate::reflection::traits::ClassInfo for #identificator {
            fn name(&self) -> String {
                #name.to_string()
            }

            fn signature(&self) -> u32 {
                #signature
            }
        }
    }
}
