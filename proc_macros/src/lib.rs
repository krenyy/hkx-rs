mod classinfo;
mod readable;
mod writable;

use classinfo::classinfo;
use proc_macro::TokenStream;
use quote::{quote, quote_spanned};
use readable::hkxbinaryreadable;
use syn::{parse_macro_input, spanned::Spanned, Data, DeriveInput, Fields, Meta};
use writable::hkxbinarywritable;

#[proc_macro_derive(HkxBinaryReadable, attributes(assert, pad, info))]
pub fn derive_hkxbinaryreadable(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    hkxbinaryreadable(input).into()
}

#[proc_macro_derive(HkxBinaryWritable, attributes(pad))]
pub fn derive_hkxbinarywritable(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    hkxbinarywritable(input).into()
}

#[proc_macro_derive(ClassInfo, attributes(signature))]
pub fn derive_classinfo(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    classinfo(input).into()
}

#[proc_macro_attribute]
pub fn hk_class(_attrs: TokenStream, input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    let vis = &input.vis;
    let ident = &input.ident;
    let fields = match &input.data {
        Data::Struct(st) => match &st.fields {
            Fields::Named(nf) => {
                let fields = &nf.named;
                quote!(#fields)
            }
            Fields::Unnamed(_uf) => quote_spanned! {
                st.struct_token.span() =>
                compile_error!("cannot be a struct with unnamed fields!")
            },
            Fields::Unit => quote_spanned! {
                st.struct_token.span() =>
                compile_error!("cannot be a unit struct!")
            },
        },
        Data::Enum(en) => quote_spanned! {
            en.enum_token.span() =>
            compile_error!("cannot be an enum!")
        },
        Data::Union(un) => quote_spanned! {
            un.union_token.span() =>
            compile_error!("cannot be a union!")
        },
    };

    let mut passthrough_attrs = vec![];

    let mut parent = quote!();
    let mut descendants = quote! {
        crate::make_enum_with_descendants!(#ident,);
    };
    let compile_errors = input
        .attrs
        .iter()
        .map(|a| {
            let parsed_meta = a.parse_meta().expect("failed to parse meta");
            let name = parsed_meta.path().get_ident().unwrap().to_string();

            match parsed_meta {
                Meta::List(li) => {
                    let items = li.nested;
                    match &*name {
                        "parent" => {
                            if items.len() == 1 {
                                let item = items.first().unwrap();
                                parent = quote! {
                                    pub base: crate::reflection::classes::#item::#item,
                                };
                                quote!()
                            } else {
                                quote_spanned! {
                                    items.span() =>
                                    compile_error!("'parent' attribute must have only one argument");
                                }
                            }
                        }
                        "descendants" => {
                            if items.len() >= 1 {
                                descendants = quote! {
                                    crate::make_enum_with_descendants!(#ident, #items);
                                };
                                quote!()
                            } else {
                                quote_spanned! {
                                    items.span() =>
                                    compile_error!("'descendants' attribute cannot be empty!");
                                }
                            }
                        }
                        _ => {
                            passthrough_attrs.push(a);
                            quote!()
                        }
                    }
                }
                _ => {
                    passthrough_attrs.push(a);
                    quote!()
                }
            }
        })
        .collect::<Vec<_>>();

    let derives = quote! {
        #[derive(
            Clone,
            Debug,
            Default,
            PartialEq,
            proc_macros::HkxBinaryReadable,
            proc_macros::HkxBinaryWritable, proc_macros::ClassInfo
        )]
    };

    quote! {
        #derives
        #(#passthrough_attrs)*
        #vis struct #ident {
            #parent
            #fields
        }
        #descendants
        #(#compile_errors)*
    }
    .into()
}
