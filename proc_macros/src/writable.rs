use proc_macro2::{Ident, TokenStream};
use quote::{quote, quote_spanned};
use syn::{spanned::Spanned, Data, DataEnum, DataStruct, DeriveInput, Field, Fields, Meta};

pub fn hkxbinarywritable(input: DeriveInput) -> TokenStream {
    let identificator = &input.ident;

    let implementation = match &input.data {
        Data::Struct(st) => impl_for_struct(st, identificator, &input),
        Data::Enum(en) => impl_for_enum(en, identificator),
        Data::Union(un) => quote_spanned! {
            un.union_token.span() =>
            compile_error!("cannot use on unions!")
        },
    };

    quote! {
        #[automatically_derived]
        impl crate::binary::traits::HkxBinaryWritable for #identificator {
            fn binary_write(self, bw: &mut crate::binary::writer::HkxBinaryWriter, ser: &mut crate::serialize::SerializationData) {
                #implementation
            }
        }
    }
}

fn impl_for_struct(st: &DataStruct, identificator: &Ident, input: &DeriveInput) -> TokenStream {
    match &st.fields {
        Fields::Named(fields) => {
            let declarations = fields.named.iter().map(|field| {
                let field_name = &field.ident;
                let field_type = &field.ty;

                let current = quote! {
                    bw.write(ser, self.#field_name);
                };

                let mut before: Vec<TokenStream> = vec![];
                let mut after: Vec<TokenStream> = vec![];

                handle_field_attributes(field, identificator, &mut before, &mut after);

                quote! {
                    #(#before)*
                    #current
                    #(#after)*
                }
            });

            let align: Vec<_> = input
                .attrs
                .iter()
                .map(|a| {
                    if let Meta::List(li) = a.parse_meta().unwrap() {
                        let items = li.nested;
                        if li.path.get_ident().unwrap().to_string() == "pad" {
                            return quote! {
                                bw.align(#items, 0);
                            };
                        }
                        return quote!();
                    }
                    return quote!();
                })
                .collect();

            quote! {
                let pos_before_writing_struct = bw.position();
                #(#declarations)*
                #(#align)*;
            }
        }
        Fields::Unnamed(fields) => {
            quote_spanned! {
                fields.span() =>
                compile_error!("cannot use on unnamed field structs!")
            }
        }
        Fields::Unit => {
            quote_spanned! {
                identificator.span() =>
                compile_error!("cannot use on unit structs!")
            }
        }
    }
}

fn handle_field_attributes(
    field: &Field,
    identificator: &Ident,
    before: &mut Vec<TokenStream>,
    after: &mut Vec<TokenStream>,
) {
    let identificator_string = identificator.to_string();

    let field_name = field.ident.as_ref().unwrap();
    let field_string = field_name.to_string();

    field
        .attrs
        .iter()
        .for_each(|attr| match attr.parse_meta().unwrap() {
            Meta::Path(path) => {
                let name = path.get_ident().unwrap().to_string();

                match &*name {
                    "pad" => before.push(quote! {
                        if ser.layout_rules.padding_option == 1 {
                            bw.align(ser.layout_rules.pointer_size.try_into().unwrap(), 0)
                        }
                    }),
                    _ => (),
                }
            }
            Meta::List(li) => {
                let name = li.path.get_ident().unwrap().to_string();
                let items = li.nested;

                match &*name {
                    "pad" => before.push(quote! {
                        bw.align(#items, 0);
                    }),
                    _ => (),
                }
            }
            _ => before.push(quote_spanned! {
                attr.span() =>
                compile_error!("invalid attribute!")
            }),
        });
}

fn impl_for_enum(en: &DataEnum, identificator: &Ident) -> TokenStream {
    let variants: Vec<TokenStream> = en
        .variants
        .iter()
        .map(|v| {
            let ident = &v.ident;
            let name = ident.to_string();

            quote! {
                Self::#ident(object) => {
                    // let object = ::std::rc::Rc::try_unwrap(object).expect("not a unique ptr!").into_inner();
                    let object = (*object).clone().into_inner();
                    let key = (crate::reflection::traits::ClassInfo::name(&object), crate::reflection::traits::ClassInfo::signature(&object));
                    if ser.class_map.contains_key(&key) {
                        ser.class_map.get_mut(&key).unwrap().push(pos);
                    } else {
                        ser.class_map.insert(key, vec![pos]);
                    }
                    bw.write(ser, object)
                },
            }
        })
        .collect();

    let enum_name = identificator.to_string();

    quote! {
        let pos: u32 = bw.position().try_into().unwrap();

        match self {
            #(#variants)*
            _ => panic!("idkidk")
        }
    }
}
