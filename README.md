### If you're not on <https://git.kroner.dev/kreny/hkx-rs>, then you're on a mirror.

# A Havok library for BotW

## What works

deserialization and serialization of all vanilla files on both (WiiU, Switch)
platforms

## What doesn't work

no idea

### Some improvements that could be made:

- [ ] make the library user-friendly (remove panics, create error types, ...)
- [ ] fix file padding (files currently don't match byte-to-byte)
- [ ] improve macro magic if possible (currently a mess)
