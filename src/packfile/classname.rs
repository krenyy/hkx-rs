use proc_macros::{HkxBinaryReadable, HkxBinaryWritable};

#[derive(Clone, Debug, HkxBinaryReadable, HkxBinaryWritable)]
pub(crate) struct HkxClassname {
    pub(crate) signature: u32,
    #[assert(0x09)]
    pub(crate) delimiter: u8,
    pub(crate) name: String,
}

impl HkxClassname {
    pub(crate) fn new(name: String, signature: u32) -> Self {
        Self {
            signature,
            delimiter: 0x09,
            name,
        }
    }
}
