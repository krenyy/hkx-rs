use crate::binary::types::FixedSizeString;
use proc_macros::{HkxBinaryReadable, HkxBinaryWritable};

#[derive(Debug, HkxBinaryReadable, HkxBinaryWritable)]
pub(crate) struct Section {
    pub(crate) section_tag: FixedSizeString<19>,
    #[assert('ÿ')]
    pub(crate) null_byte: char,
    pub(crate) absolute_data_start: i32,
    pub(crate) local_fixups_offset: i32,
    pub(crate) global_fixups_offset: i32,
    pub(crate) virtual_fixups_offset: i32,
    pub(crate) exports_offset: i32,
    pub(crate) imports_offset: i32,
    pub(crate) end_offset: i32,
    #[assert(-1, -1, -1, -1)]
    _pad: [i32; 4],
}

impl Section {
    pub fn new(name: &str) -> Self {
        Self {
            section_tag: FixedSizeString(name.to_string()),
            null_byte: 'ÿ',
            absolute_data_start: 0,
            local_fixups_offset: 0,
            global_fixups_offset: 0,
            virtual_fixups_offset: 0,
            exports_offset: 0,
            imports_offset: 0,
            end_offset: 0,
            _pad: [-1, -1, -1, -1],
        }
    }
}
