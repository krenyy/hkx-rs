use proc_macros::{HkxBinaryReadable, HkxBinaryWritable};

#[derive(Debug, HkxBinaryReadable, HkxBinaryWritable)]
pub struct Predicate {
    #[assert(20)]
    unk_40: i16,
    #[assert(0)]
    unk_42: i16,
    #[assert(0)]
    unk_44: u32,
    #[assert(0)]
    unk_48: u32,
    #[assert(0)]
    unk_4c: u32,
}

impl Predicate {
    pub fn new() -> Self {
        Self {
            unk_40: 0x14,
            unk_42: 0,
            unk_44: 0,
            unk_48: 0,
            unk_4c: 0,
        }
    }
}
