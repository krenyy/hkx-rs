pub(crate) mod layout_rules;
pub(crate) mod predicate;
pub(crate) mod variant;

use self::{layout_rules::LayoutRules, variant::Variant};
use crate::binary::{reader::HkxBinaryReader, traits::HkxBinaryReadable, types::FixedSizeString};
use proc_macros::{HkxBinaryReadable, HkxBinaryWritable};

#[derive(Debug, HkxBinaryReadable, HkxBinaryWritable)]
pub(crate) struct Header {
    #[assert(0x57e0e057, 0x10c0c010)]
    pub(crate) magic: [u32; 2],
    #[assert(0)]
    pub(crate) user_tag: i32,
    #[assert(11)]
    pub(crate) file_version: i32,
    pub(crate) layout_rules: LayoutRules,
    #[assert(3)]
    pub(crate) num_sections: i32,
    #[assert(2)]
    pub(crate) contents_section_index: i32,
    #[assert(0)]
    pub(crate) contents_section_offset: i32,
    #[assert(0)]
    pub(crate) contents_class_name_section_index: i32,
    #[assert(75)]
    pub(crate) contents_class_name_section_offset: i32,
    pub(crate) contents_version: [char; 16],
    #[assert(0)]
    pub(crate) flags: i32,
    #[assert(21)]
    pub(crate) max_predicate: u16,
    pub(crate) predicate_array_size_plus_padding: u16,
}

impl Header {
    pub(crate) fn new(variant: Variant) -> Self {
        Self {
            magic: [0x57e0e057, 0x10c0c010],
            user_tag: 0,
            file_version: 11,
            layout_rules: LayoutRules::new(variant),
            num_sections: 3,
            contents_section_index: 2,
            contents_section_offset: 0,
            contents_class_name_section_index: 0,
            contents_class_name_section_offset: 0x4b,
            contents_version: "hk_2014.2.0-r1\0ÿ"
                .to_string()
                .chars()
                .collect::<Vec<char>>()
                .try_into()
                .unwrap(),
            flags: 0,
            max_predicate: 0x15,
            predicate_array_size_plus_padding: 0,
        }
    }
}
