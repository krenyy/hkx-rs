use proc_macros::{HkxBinaryReadable, HkxBinaryWritable};

use super::variant::Variant;

#[derive(Clone, Debug, HkxBinaryReadable, HkxBinaryWritable)]
pub(crate) struct LayoutRules {
    pub(crate) pointer_size: i8,
    pub(crate) endian: i8,
    pub(crate) padding_option: i8,
    pub(crate) base_class: i8,
}

impl LayoutRules {
    pub(crate) fn new(variant: Variant) -> Self {
        match variant {
            Variant::WiiU => Self {
                pointer_size: 4,
                endian: 0,
                padding_option: 0,
                base_class: 1,
            },
            Variant::Switch => Self {
                pointer_size: 8,
                endian: 1,
                padding_option: 1,
                base_class: 1,
            },
        }
    }
}
