use proc_macros::{HkxBinaryReadable, HkxBinaryWritable};

#[derive(Debug, HkxBinaryReadable, HkxBinaryWritable)]
pub(crate) struct LocalFixup {
    pub(crate) src: u32,
    pub(crate) dst: u32,
}

#[derive(Debug, HkxBinaryReadable, HkxBinaryWritable)]
pub(crate) struct GlobalFixup {
    pub(crate) src: u32,
    #[assert(2)]
    pub(crate) dst_section_id: u32,
    pub(crate) dst: u32,
}

#[derive(Debug, HkxBinaryReadable, HkxBinaryWritable)]
pub(crate) struct VirtualFixup {
    pub(crate) src: u32,
    #[assert(0)]
    pub(crate) dst_section_id: u32,
    pub(crate) dst: u32,
}

pub(crate) trait Fixup {
    fn get_src(&self) -> u32;
}

impl Fixup for LocalFixup {
    fn get_src(&self) -> u32 {
        self.src
    }
}

impl Fixup for GlobalFixup {
    fn get_src(&self) -> u32 {
        self.src
    }
}

impl Fixup for VirtualFixup {
    fn get_src(&self) -> u32 {
        self.src
    }
}
