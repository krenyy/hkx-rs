use crate::{
    binary::{enums::Endian, writer::HkxBinaryWriter},
    packfile::{
        classname::HkxClassname,
        fixups::{GlobalFixup, LocalFixup, VirtualFixup},
        header::{layout_rules::LayoutRules, predicate::Predicate, variant::Variant, Header},
        section::Section,
    },
    reflection::{classes::hkReferencedObject::E_hkReferencedObject, HkObject},
};
use std::collections::{HashMap, VecDeque};

pub(crate) struct SerializationData {
    pub(crate) layout_rules: LayoutRules,
    pub(crate) current_local_queue: usize,
    pub(crate) local_queues: Vec<
        VecDeque<(
            u32,
            Box<dyn FnOnce(&mut HkxBinaryWriter, &mut SerializationData) -> u32>,
        )>,
    >,

    pub(crate) global_queue: VecDeque<(Vec<u32>, HkObject)>,
    pub(crate) class_map: HashMap<(String, u32), Vec<u32>>,
    pub(crate) serialized_objects: Vec<(HkObject, Vec<u32>, u32)>,
}

impl SerializationData {
    pub(crate) fn push_local_queue(&mut self) {
        self.current_local_queue += 1;
        if self.current_local_queue == self.local_queues.len() {
            self.local_queues.push(VecDeque::new())
        }
    }

    pub(crate) fn pop_local_queue(&mut self) {
        self.current_local_queue -= 1;
    }
}

pub fn serialize(roots: Vec<HkObject>, variant: Variant) -> Vec<u8> {
    let mut bytes = vec![];

    let mut should_have_predicate = false;
    let mut next_should_have_predicate = false;
    for root in roots {
        // check if current or next file should have predicate (unimportant, for good measure)
        match &root {
            HkObject::hkRootLevelContainer(rlc) => {
                let rlc = rlc.borrow();
                if !rlc.namedVariants.is_empty() {
                    match rlc.namedVariants[0].variant.as_ref().unwrap() {
                        E_hkReferencedObject::hkpRigidBody(_)
                        | E_hkReferencedObject::hkaiNavMesh(_) => should_have_predicate = true,
                        _ => (),
                    }
                }
            }
            HkObject::StaticCompoundInfo(sci) => {
                let mut sci = sci.borrow_mut();
                let actor_info_len: u32 = sci.ActorInfo.len().try_into().unwrap();
                let shape_info_len: u32 = sci.ShapeInfo.len().try_into().unwrap();
                sci.Offset = actor_info_len * 16
                    + shape_info_len * 12
                    + 368
                    + if actor_info_len > 0 { 8 } else { 0 }
                    + if shape_info_len > 0 { 8 } else { 0 }
                    + match variant {
                        Variant::Switch => 48,
                        Variant::WiiU => 32,
                    };
                let rem = sci.Offset % 16;
                if rem > 0 {
                    sci.Offset += 16 - rem;
                }
                next_should_have_predicate = true
            }
            _ => (),
        }

        let mut header = Header::new(variant.clone());

        let mut ser = SerializationData {
            layout_rules: header.layout_rules.clone(),
            current_local_queue: 0,
            local_queues: vec![VecDeque::new()],
            global_queue: VecDeque::new(),
            class_map: HashMap::new(),
            serialized_objects: vec![],
        };

        let endian = match variant {
            Variant::WiiU => Endian::Big,
            Variant::Switch => Endian::Little,
        };

        let mut bw = HkxBinaryWriter::new(endian);
        let mut class_bw = HkxBinaryWriter::new(endian);
        let mut data_bw = HkxBinaryWriter::new(endian);

        let mut classnames = Section::new("__classnames__");
        let mut types = Section::new("__types__");
        let mut data = Section::new("__data__");

        let mut local_fixups = vec![];
        let mut global_fixups = vec![];
        let mut virtual_fixups = vec![];

        ser.global_queue.push_back((vec![0], root));

        while !ser.global_queue.is_empty() {
            let (sources, object) = ser.global_queue.pop_back().unwrap();

            let dst: u32 = data_bw.position().try_into().unwrap();
            sources.into_iter().for_each(|src| {
                global_fixups.push(GlobalFixup {
                    src,
                    dst_section_id: 2,
                    dst,
                })
            });

            ser.serialized_objects.push((object.clone(), vec![], dst));
            data_bw.write(&mut ser, object);
            data_bw.align(16, 0);

            while !ser.local_queues.is_empty() {
                let mut queue = ser.local_queues.pop().unwrap();
                while !queue.is_empty() {
                    let (src, action) = queue.pop_front().unwrap();

                    local_fixups.push(LocalFixup {
                        src,
                        dst: action(&mut data_bw, &mut ser),
                    });
                }
                data_bw.align(16, 0);
            }
        }

        ser.serialized_objects
            .iter()
            .for_each(|(_object, sources, dst)| {
                sources.iter().for_each(|src| {
                    global_fixups.push(GlobalFixup {
                        src: *src,
                        dst_section_id: 2,
                        dst: *dst,
                    })
                })
            });

        class_bw.write(
            &mut ser,
            HkxClassname::new("hkClass".to_string(), 0x33d42383),
        );
        class_bw.write(
            &mut ser,
            HkxClassname::new("hkClassMember".to_string(), 0xb0efa719),
        );
        class_bw.write(
            &mut ser,
            HkxClassname::new("hkClassEnum".to_string(), 0x8a3609cf),
        );
        class_bw.write(
            &mut ser,
            HkxClassname::new("hkClassEnumItem".to_string(), 0xce6f8a6c),
        );
        let class_map = ser.class_map.clone();
        let mut sorted_classes = class_map
            .into_iter()
            .map(|((name, signature), sources)| {
                let mut sources = sources;
                sources.sort();
                (name, signature, sources)
            })
            .collect::<Vec<_>>();
        sorted_classes.sort_by_key(|(_name, _signature, sources)| *sources.first().unwrap());
        sorted_classes
            .iter()
            .for_each(|(name, signature, sources)| {
                let pos: u32 = class_bw.position().try_into().unwrap();
                let dst = pos + 5;
                class_bw.write(&mut ser, HkxClassname::new(name.clone(), *signature));
                sources.iter().for_each(|src| {
                    virtual_fixups.push(VirtualFixup {
                        src: *src,
                        dst_section_id: 0,
                        dst,
                    })
                })
            });
        class_bw.align(16, 0xff);

        classnames.absolute_data_start = 0x100;
        if should_have_predicate {
            header.predicate_array_size_plus_padding = 0x10;
            classnames.absolute_data_start = 0x110;
        }

        let class_bw_bytes = class_bw.to_bytes();
        let class_bw_bytes_len: i32 = class_bw_bytes.len().try_into().unwrap();
        classnames.local_fixups_offset = class_bw_bytes_len;
        classnames.global_fixups_offset = class_bw_bytes_len;
        classnames.virtual_fixups_offset = class_bw_bytes_len;
        classnames.exports_offset = class_bw_bytes_len;
        classnames.imports_offset = class_bw_bytes_len;
        classnames.end_offset = class_bw_bytes_len;

        types.absolute_data_start = classnames.absolute_data_start + class_bw_bytes_len;
        data.absolute_data_start = classnames.absolute_data_start + class_bw_bytes_len;

        data.local_fixups_offset = data_bw.position().try_into().unwrap();
        local_fixups
            .into_iter()
            .for_each(|fixup| data_bw.write(&mut ser, fixup));
        data_bw.align(16, 0xff);

        data.global_fixups_offset = data_bw.position().try_into().unwrap();
        global_fixups
            .into_iter()
            .skip(1) // dummy global fixup (points from 0 to 0)
            .for_each(|fixup| data_bw.write(&mut ser, fixup));
        data_bw.align(16, 0xff);

        data.virtual_fixups_offset = data_bw.position().try_into().unwrap();
        virtual_fixups
            .into_iter()
            .for_each(|fixup| data_bw.write(&mut ser, fixup));
        data_bw.align(16, 0xff);

        let data_bw_bytes = data_bw.to_bytes();
        let data_bw_bytes_len: i32 = data_bw_bytes.len().try_into().unwrap();
        data.exports_offset = data_bw_bytes_len;
        data.imports_offset = data_bw_bytes_len;
        data.end_offset = data_bw_bytes_len;

        bw.write(&mut ser, header);
        if should_have_predicate {
            bw.write(&mut ser, Predicate::new());
        }
        bw.write(&mut ser, classnames);
        bw.write(&mut ser, types);
        bw.write(&mut ser, data);

        bw.write(&mut ser, class_bw_bytes);
        bw.write(&mut ser, data_bw_bytes);

        bytes.extend(bw.to_bytes());

        should_have_predicate = next_should_have_predicate
    }

    bytes
}
