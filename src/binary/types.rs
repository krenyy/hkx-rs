use std::{
    fmt::Display,
    ops::{Deref, DerefMut},
};

pub type f16 = half::f16;
pub type Vector4 = nalgebra::Vector4<f32>;
pub type Matrix3 = nalgebra::Matrix3x4<f32>;
pub type QsTransform = Matrix3;
pub type Matrix4 = nalgebra::Matrix4<f32>;
pub type Transform = Matrix4;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct FixedSizeString<const N: usize>(pub(crate) String);

impl<const N: usize> Display for FixedSizeString<N> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl<const N: usize> Deref for FixedSizeString<N> {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<const N: usize> DerefMut for FixedSizeString<N> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct VoidPointer;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct VoidArray;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct StringPointer(pub(crate) String);

impl Deref for StringPointer {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for StringPointer {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct GlobalPointer<T>(pub(crate) Option<T>);

impl<T> Default for GlobalPointer<T> {
    fn default() -> Self {
        Self(None)
    }
}

impl<T> Deref for GlobalPointer<T> {
    type Target = Option<T>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> DerefMut for GlobalPointer<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Array<T>(pub(crate) Vec<T>);

impl<T> Default for Array<T> {
    fn default() -> Self {
        Self(vec![])
    }
}

impl<T> Deref for Array<T> {
    type Target = Vec<T>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> DerefMut for Array<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
