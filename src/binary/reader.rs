use super::{
    enums::{Endian, SeekMode},
    traits::HkxBinaryReadable,
};
use crate::{
    packfile::{
        classname::HkxClassname,
        fixups::{GlobalFixup, LocalFixup, VirtualFixup},
        header::{layout_rules::LayoutRules, variant::Variant},
    },
    reflection::HkObject,
};
use std::collections::{HashMap, VecDeque};

pub(crate) struct DeserializationData {
    pub(crate) layout_rules: LayoutRules,
    pub(crate) data_offset: u32,
    pub(crate) local_fixups: HashMap<u32, LocalFixup>,
    pub(crate) global_fixups: HashMap<u32, GlobalFixup>,
    pub(crate) virtual_fixups: HashMap<u32, VirtualFixup>,
    pub(crate) classnames: HashMap<u32, HkxClassname>,
    pub(crate) objects: HashMap<u32, HkObject>,
}

pub(crate) struct HkxBinaryReader<'a> {
    position: usize,
    endian: Endian,
    steps: VecDeque<usize>,
    pub(crate) bytes: &'a [u8],
    pub(crate) des: DeserializationData,
}

impl<'a> HkxBinaryReader<'a> {
    pub(crate) fn from_bytes(bytes: &'a [u8]) -> Self {
        Self {
            position: 0,
            endian: Endian::Big,
            steps: VecDeque::new(),
            bytes,
            des: DeserializationData {
                layout_rules: LayoutRules::new(Variant::WiiU),
                data_offset: 0,
                local_fixups: HashMap::new(),
                global_fixups: HashMap::new(),
                virtual_fixups: HashMap::new(),
                classnames: HashMap::new(),
                objects: HashMap::new(),
            },
        }
    }

    pub(crate) fn position(&self) -> usize {
        self.position
    }

    pub(crate) fn set_position(&mut self, pos: usize) {
        let length = self.length();
        if !(0..length).contains(&pos) {
            panic!(
                "Tried to set position outside slice range! ({}/{})",
                pos, length
            );
        }
        self.position = pos
    }

    pub(crate) fn endian(&self) -> &Endian {
        &self.endian
    }

    pub(crate) fn set_endian(&mut self, endian: Endian) {
        self.endian = endian
    }

    pub(crate) fn length(&self) -> usize {
        self.bytes.len()
    }

    pub(crate) fn is_at_end(&self) -> bool {
        self.position == self.bytes.len()
    }

    pub(crate) fn align(&mut self, n: usize) {
        if self.position % n == 0 {
            return;
        }
        self.position += n - (self.position % n)
    }

    pub(crate) fn seek(&mut self, pos: usize, mode: SeekMode) {
        let cur_pos = self.position();
        let length = self.length();
        match mode {
            SeekMode::Absolute => {
                if pos > length {
                    panic!("Cannot seek beyond the stream end! ({}/{})", pos, length);
                }
                self.position = pos
            }
            SeekMode::Forward => {
                if pos > (length - cur_pos) {
                    panic!(
                        "Cannot seek beyond the stream end! ({}/{})",
                        cur_pos + pos,
                        length
                    );
                }
                self.position += pos
            }
            SeekMode::Backward => {
                if pos > cur_pos {
                    panic!(
                        "Cannot seek beyond the stream beginning! (-{}/{})",
                        pos - cur_pos,
                        length
                    )
                }
                self.position -= pos
            }
        }
    }

    pub(crate) fn step_in(&mut self, pos: usize, mode: SeekMode) {
        self.steps.push_front(self.position);
        self.seek(pos, mode)
    }

    pub(crate) fn step_out(&mut self) {
        if self.steps.len() == 0 {
            panic!("Already stepped all the way out!")
        }

        self.position = self.steps.pop_front().unwrap()
    }

    pub(crate) fn read_byte(&mut self) -> u8 {
        let ret = self.bytes[self.position];
        self.seek(1, SeekMode::Forward);
        ret
    }

    pub(crate) fn peek<T>(&mut self) -> T
    where
        T: HkxBinaryReadable,
    {
        self.step_in(self.position(), SeekMode::Absolute);
        let ret = self.read();
        self.step_out();
        ret
    }

    pub(crate) fn read<T>(&mut self) -> T
    where
        T: HkxBinaryReadable,
    {
        T::binary_read(self)
    }
}
