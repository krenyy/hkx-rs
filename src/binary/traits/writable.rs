use std::collections::VecDeque;

use crate::{
    binary::{
        enums::Endian,
        types::{
            f16, Array, FixedSizeString, GlobalPointer, Matrix3, Matrix4, StringPointer, Vector4,
            VoidArray, VoidPointer,
        },
        writer::HkxBinaryWriter,
    },
    reflection::HkObject,
    serialize::SerializationData,
};

pub(crate) trait HkxBinaryWritable {
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData);
}

impl HkxBinaryWritable for u8 {
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        bw.write_byte(self);
    }
}

impl HkxBinaryWritable for char {
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        bw.write(ser, self as u8);
    }
}

impl HkxBinaryWritable for bool {
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        match self {
            true => bw.write::<u8>(ser, 1),
            false => bw.write::<u8>(ser, 0),
        }
    }
}

macro_rules! impl_for_primitive {
    ($type:tt, $num:expr) => {
        impl HkxBinaryWritable for $type {
            fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
                bw.write(
                    ser,
                    match bw.endian() {
                        Endian::Big => self.to_be_bytes(),
                        Endian::Little => self.to_le_bytes(),
                    },
                )
            }
        }
    };
}

macro_rules! impl_for_signed_primitive {
    ($type:tt, $unsigned_type:tt) => {
        impl HkxBinaryWritable for $type {
            fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
                bw.write(ser, self as $unsigned_type)
            }
        }
    };
}

impl_for_primitive!(f16, 2);
impl_for_primitive!(f32, 4);
impl_for_primitive!(f64, 8);

impl_for_primitive!(u16, 2);
impl_for_primitive!(u32, 4);
impl_for_primitive!(u64, 8);
impl_for_primitive!(u128, 16);

impl_for_signed_primitive!(i8, u8);
impl_for_signed_primitive!(i16, u16);
impl_for_signed_primitive!(i32, u32);
impl_for_signed_primitive!(i64, u64);
impl_for_signed_primitive!(i128, u128);

impl HkxBinaryWritable for usize {
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        let pointer_size = ser.layout_rules.pointer_size;

        bw.align(pointer_size.try_into().unwrap(), 0);

        match pointer_size {
            4 => bw.write::<u32>(ser, self.try_into().unwrap()),
            8 => bw.write::<u64>(ser, self.try_into().unwrap()),
            _ => panic!("invalid pointer size!"),
        }
    }
}

impl HkxBinaryWritable for String {
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        bw.write(ser, self.as_bytes().to_vec());
        bw.write(ser, 0u8);
    }
}

impl<const N: usize> HkxBinaryWritable for FixedSizeString<N> {
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        let string = self.0;
        let string_length = string.len();
        bw.write(ser, string);
        for _ in 0..(N - string_length - 1) {
            bw.write(ser, 0u8);
        }
    }
}

impl<T> HkxBinaryWritable for Vec<T>
where
    T: HkxBinaryWritable,
{
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        self.into_iter().for_each(|item| bw.write(ser, item))
    }
}

impl<T, const N: usize> HkxBinaryWritable for [T; N]
where
    T: HkxBinaryWritable + Default,
{
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        for item in self {
            bw.write(ser, item)
        }
    }
}

fn write_pointer(bw: &mut HkxBinaryWriter, ser: &mut SerializationData) -> u32 {
    let pointer_size: u32 = ser.layout_rules.pointer_size.try_into().unwrap();
    bw.write::<usize>(ser, 0);

    let pos: u32 = bw.position().try_into().unwrap();
    let src = pos - pointer_size;

    src
}

impl HkxBinaryWritable for VoidPointer {
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        write_pointer(bw, ser);
    }
}

impl HkxBinaryWritable for StringPointer {
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        let src = write_pointer(bw, ser);

        while ser.current_local_queue >= ser.local_queues.len() {
            ser.local_queues.push(VecDeque::new())
        }
        ser.local_queues[ser.current_local_queue].push_back((
            src,
            Box::new(|bw: &mut HkxBinaryWriter, ser: &mut SerializationData| {
                let dst: u32 = bw.position().try_into().unwrap();
                bw.write(ser, self.0);
                bw.align(16, 0);
                dst
            }),
        ));
    }
}

impl<T> HkxBinaryWritable for GlobalPointer<T>
where
    T: HkxBinaryWritable + Into<HkObject> + From<HkObject> + Clone,
{
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        let src = write_pointer(bw, ser);

        match &*self {
            Some(object) => {
                let self_object: HkObject = object.clone().into();

                let found = ser
                    .serialized_objects
                    .iter_mut()
                    .find(|(object, _sources, _dst)| self_object == *object);

                if let Some((_object, sources, _dst)) = found {
                    sources.push(src)
                } else {
                    ser.global_queue.push_front((vec![src], self_object))
                }
            }
            None => (),
        }
    }
}

fn write_array_count(bw: &mut HkxBinaryWriter, ser: &mut SerializationData, count: u32) {
    bw.write(ser, count);
    bw.write(ser, count | (0x80 << 24));
}

impl HkxBinaryWritable for VoidArray {
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        write_pointer(bw, ser);
        write_array_count(bw, ser, 0);
    }
}

impl<T> HkxBinaryWritable for Array<T>
where
    T: HkxBinaryWritable + 'static,
{
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        let src = write_pointer(bw, ser);
        write_array_count(bw, ser, self.len().try_into().unwrap());

        if self.len() == 0 {
            return;
        }

        while ser.current_local_queue >= ser.local_queues.len() {
            ser.local_queues.push(VecDeque::new())
        }
        ser.local_queues[ser.current_local_queue].push_back((
            src,
            Box::new(|bw: &mut HkxBinaryWriter, ser: &mut SerializationData| {
                bw.align(16, 0);
                ser.push_local_queue();
                let dst: u32 = bw.position().try_into().unwrap();
                bw.write(ser, self.0);
                ser.pop_local_queue();
                dst
            }),
        ));
    }
}

impl HkxBinaryWritable for Vector4 {
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        bw.align(16, 0);
        bw.write(ser, self.x);
        bw.write(ser, self.y);
        bw.write(ser, self.z);
        bw.write(ser, self.w);
    }
}

impl HkxBinaryWritable for Matrix3 {
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        bw.align(16, 0);
        bw.write(ser, self.m11);
        bw.write(ser, self.m12);
        bw.write(ser, self.m13);
        bw.write(ser, self.m14);
        bw.write(ser, self.m21);
        bw.write(ser, self.m22);
        bw.write(ser, self.m23);
        bw.write(ser, self.m24);
        bw.write(ser, self.m31);
        bw.write(ser, self.m32);
        bw.write(ser, self.m33);
        bw.write(ser, self.m34);
    }
}

impl HkxBinaryWritable for Matrix4 {
    fn binary_write(self, bw: &mut HkxBinaryWriter, ser: &mut SerializationData) {
        bw.align(16, 0);
        bw.write(ser, self.m11);
        bw.write(ser, self.m12);
        bw.write(ser, self.m13);
        bw.write(ser, self.m14);
        bw.write(ser, self.m21);
        bw.write(ser, self.m22);
        bw.write(ser, self.m23);
        bw.write(ser, self.m24);
        bw.write(ser, self.m31);
        bw.write(ser, self.m32);
        bw.write(ser, self.m33);
        bw.write(ser, self.m34);
        bw.write(ser, self.m41);
        bw.write(ser, self.m42);
        bw.write(ser, self.m43);
        bw.write(ser, self.m44);
    }
}
