use crate::{
    binary::{
        enums::{Endian, SeekMode},
        reader::HkxBinaryReader,
        types::{
            f16, Array, FixedSizeString, GlobalPointer, Matrix3, Matrix4, StringPointer, Vector4,
            VoidArray, VoidPointer,
        },
    },
    reflection::HkObject,
};

pub(crate) trait HkxBinaryReadable {
    fn binary_read(br: &mut HkxBinaryReader) -> Self;
}

impl HkxBinaryReadable for u8 {
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        br.read_byte()
    }
}

impl HkxBinaryReadable for char {
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        br.read::<u8>() as char
    }
}

impl HkxBinaryReadable for bool {
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        match br.read::<u8>() {
            0 => false,
            1 => true,
            _ => panic!("invalid value for type bool!"),
        }
    }
}

macro_rules! impl_for_primitive {
    ($type:tt, $num:expr) => {
        impl HkxBinaryReadable for $type {
            fn binary_read(br: &mut HkxBinaryReader) -> Self {
                let bytes = br.read::<[u8; $num]>();
                match br.endian() {
                    Endian::Big => $type::from_be_bytes(bytes),
                    Endian::Little => $type::from_le_bytes(bytes),
                }
            }
        }
    };
}

macro_rules! impl_for_signed_primitive {
    ($type:tt, $unsigned_type:tt) => {
        impl HkxBinaryReadable for $type {
            fn binary_read(br: &mut HkxBinaryReader) -> Self {
                $unsigned_type::binary_read(br) as $type
            }
        }
    };
}

impl_for_primitive!(f16, 2);
impl_for_primitive!(f32, 4);
impl_for_primitive!(f64, 8);

impl_for_primitive!(u16, 2);
impl_for_primitive!(u32, 4);
impl_for_primitive!(u64, 8);
impl_for_primitive!(u128, 16);

impl_for_signed_primitive!(i8, u8);
impl_for_signed_primitive!(i16, u16);
impl_for_signed_primitive!(i32, u32);
impl_for_signed_primitive!(i64, u64);
impl_for_signed_primitive!(i128, u128);

impl HkxBinaryReadable for usize {
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        let pointer_size = br.des.layout_rules.pointer_size;

        br.align(pointer_size.try_into().unwrap());

        match pointer_size {
            4 => br.read::<u32>().try_into().unwrap(),
            8 => br.read::<u64>().try_into().unwrap(),
            _ => panic!("invalid pointer size!"),
        }
    }
}

impl HkxBinaryReadable for String {
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        let mut bytes = vec![];

        while br.peek::<u8>().is_ascii() {
            let current_byte: u8 = br.read();
            if current_byte == 0 {
                break;
            }

            bytes.push(current_byte)
        }

        if bytes.len() == 0 {
            panic!("Not a string!")
        }

        String::from_utf8(bytes).unwrap()
    }
}

impl<const N: usize> HkxBinaryReadable for FixedSizeString<N> {
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        let bytes: [u8; N] = br.read();
        let mut newbytes = vec![];

        for byte in bytes {
            if byte.is_ascii() && byte != 0 {
                newbytes.push(byte);
                continue;
            }
            break;
        }

        Self(String::from_utf8(newbytes).unwrap())
    }
}

impl<T, const N: usize> HkxBinaryReadable for [T; N]
where
    T: HkxBinaryReadable + Default + std::fmt::Debug,
{
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        let mut ret: [T; N] = (0..N)
            .map(|_| T::default())
            .collect::<Vec<_>>()
            .try_into()
            .unwrap();
        for i in 0..N {
            ret[i] = br.read()
        }
        ret
    }
}

fn read_pointer(br: &mut HkxBinaryReader) -> u32 {
    let pointer_size: u32 = br.des.layout_rules.pointer_size.try_into().unwrap();
    let value: usize = br.read();

    let pos: u32 = br.position().try_into().unwrap();
    let pos = pos - pointer_size;

    let src = pos - br.des.data_offset;

    assert_eq!(value, 0, "failed asserting pointer at pos: {:#x}", pos);

    src
}

impl HkxBinaryReadable for VoidPointer {
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        let src = read_pointer(br);

        if let Some(_) = br.des.local_fixups.get(&src) {
            panic!("local fixup exists for VoidPointer!");
        }

        if let Some(_) = br.des.global_fixups.get(&src) {
            panic!("global fixup exists for VoidPointer!");
        }

        Self
    }
}

impl HkxBinaryReadable for StringPointer {
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        let src = read_pointer(br);

        let fixup = br.des.local_fixups.get(&src).unwrap();

        br.step_in(
            (br.des.data_offset + fixup.dst).try_into().unwrap(),
            SeekMode::Absolute,
        );

        let ret = br.read();
        br.step_out();

        Self(ret)
    }
}

impl<T> HkxBinaryReadable for GlobalPointer<T>
where
    T: HkxBinaryReadable + Into<HkObject> + From<HkObject>,
{
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        let src = read_pointer(br);

        let fixup_option = br.des.global_fixups.get(&src);

        let dst;
        match fixup_option {
            Some(fixup) => dst = fixup.dst,
            None => return Self(None),
        }

        let object_option = br.des.objects.get(&dst);
        match object_option {
            Some(object) => return Self(Some(object.clone().into())),
            None => (),
        }

        br.step_in(
            (br.des.data_offset + dst).try_into().unwrap(),
            SeekMode::Absolute,
        );
        let ret = br.read::<HkObject>();
        br.step_out();

        br.des.objects.insert(dst, ret.clone());

        Self(Some(ret.into()))
    }
}

fn read_array_count(br: &mut HkxBinaryReader) -> u32 {
    let count: u32 = br.read();
    assert_eq!(
        br.read::<u32>(),
        count | (0x80 << 24),
        "count and capacity must match! pos: {:#x}",
        br.position()
    );
    count
}

impl HkxBinaryReadable for VoidArray {
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        read_pointer(br);
        let count = read_array_count(br);

        assert_eq!(count, 0, "void array must have count == 0!");

        Self
    }
}

impl<T> HkxBinaryReadable for Array<T>
where
    T: HkxBinaryReadable,
{
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        let src = read_pointer(br);
        let count = read_array_count(br);

        let mut ret = vec![];

        if count == 0 {
            return Self(ret);
        }

        let lfu = br.des.local_fixups.get(&src).unwrap();

        br.step_in(
            (br.des.data_offset + lfu.dst).try_into().unwrap(),
            SeekMode::Absolute,
        );
        for _ in 0..count {
            ret.push(br.read())
        }
        br.step_out();

        Self(ret)
    }
}

impl HkxBinaryReadable for Vector4 {
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        br.align(16);
        Vector4::new(br.read(), br.read(), br.read(), br.read())
    }
}

impl HkxBinaryReadable for Matrix3 {
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        br.align(16);
        Self::new(
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
        )
    }
}

impl HkxBinaryReadable for Matrix4 {
    fn binary_read(br: &mut HkxBinaryReader) -> Self {
        br.align(16);
        Self::new(
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
            br.read(),
        )
    }
}
