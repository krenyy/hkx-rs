#[derive(Clone, Copy, Debug)]
pub(crate) enum SeekMode {
    Absolute,
    Forward,
    Backward,
}

#[derive(Clone, Copy, Debug)]
pub(crate) enum Endian {
    Little,
    Big,
}
