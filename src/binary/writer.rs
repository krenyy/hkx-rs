use super::{enums::Endian, traits::HkxBinaryWritable};
use crate::serialize::SerializationData;

pub(crate) struct HkxBinaryWriter {
    position: usize,
    endian: Endian,
    pub(crate) bytes: Vec<u8>,
}

impl HkxBinaryWriter {
    pub(crate) fn new(endian: Endian) -> Self {
        Self {
            position: 0,
            endian,
            bytes: vec![],
        }
    }

    pub(crate) fn to_bytes(self) -> Vec<u8> {
        self.bytes
    }

    pub(crate) fn endian(&self) -> &Endian {
        &self.endian
    }

    pub(crate) fn position(&self) -> usize {
        self.position
    }

    pub(crate) fn align(&mut self, n: usize, c: u8) {
        if self.position % n == 0 {
            return;
        }
        self.position += n - (self.position % n);
        while self.position > self.bytes.len() {
            self.bytes.push(c);
        }
    }

    pub(crate) fn write_byte(&mut self, value: u8) {
        if self.position == self.bytes.len() {
            self.bytes.push(value);
        } else {
            self.bytes[self.position] = value;
        }
        self.position += 1;
    }

    pub(crate) fn write<T>(&mut self, ser: &mut SerializationData, value: T)
    where
        T: HkxBinaryWritable,
    {
        value.binary_write(self, ser)
    }
}
