use crate::{deserialize::deserialize, packfile::header::variant::Variant, serialize::serialize};
use std::fs;

static VANILLA_PACKFILE_LIST: &'static str =
    include_str!("../test_files/vanilla_packfile_list.txt");

fn canonical_to_real(path: &str, platform: &str) -> String {
    format!("test_files/{platform}/{path}")
}

fn roundtrip_compare_packfiles(path: &str, reverse: bool) {
    let nx_bytes = fs::read(canonical_to_real(path, "nx")).unwrap();
    let wiiu_bytes = fs::read(canonical_to_real(path, "wiiu")).unwrap();

    let mut nx_roots = deserialize(&nx_bytes);
    let mut wiiu_roots = deserialize(&wiiu_bytes);

    if reverse {
        (nx_roots, wiiu_roots) = (wiiu_roots, nx_roots)
    }

    let new_nx_bytes = serialize(nx_roots, Variant::Switch);
    let new_wiiu_bytes = serialize(wiiu_roots, Variant::WiiU);

    assert!(nx_bytes == new_nx_bytes);
    assert!(wiiu_bytes == new_wiiu_bytes);
}

fn filtered_by_prefix(path_prefix: &str) -> Vec<&str> {
    VANILLA_PACKFILE_LIST
        .lines()
        .filter(|p| p.starts_with(path_prefix))
        .collect()
}

fn test_roundtrip(path_prefix: &str, reverse: bool) {
    filtered_by_prefix(path_prefix).iter().for_each(|p| {
        roundtrip_compare_packfiles(p, reverse);
    })
}

#[test]
fn roundtrip_cloth() {
    test_roundtrip("Physics/Cloth", false)
}

#[test]
fn roundtrip_nav_mesh() {
    test_roundtrip("NavMesh", false)
}

#[test]
fn roundtrip_ragdoll() {
    test_roundtrip("Physics/Ragdoll", false)
}

#[test]
fn roundtrip_rigid_body() {
    test_roundtrip("Physics/RigidBody", false)
}

#[test]
fn roundtrip_static_compound() {
    test_roundtrip("Physics/StaticCompound", false)
}

#[test]
fn roundtrip_tera_mesh_rigid_body() {
    test_roundtrip("Physics/TeraMeshRigidBody", false)
}

#[ignore = "different data"]
#[test]
fn reverse_roundtrip_cloth() {
    test_roundtrip("Physics/Cloth", true)
}

#[test]
fn reverse_roundtrip_nav_mesh() {
    test_roundtrip("NavMesh", true)
}

#[test]
fn reverse_roundtrip_ragdoll() {
    test_roundtrip("Physics/Ragdoll", true)
}

#[test]
fn reverse_roundtrip_rigid_body() {
    test_roundtrip("Physics/RigidBody", true)
}

#[test]
fn reverse_roundtrip_static_compound() {
    test_roundtrip("Physics/StaticCompound", true)
}

#[test]
fn reverse_roundtrip_tera_mesh_rigid_body() {
    test_roundtrip("Physics/TeraMeshRigidBody", true)
}
