use crate::{
    binary::{
        enums::{Endian, SeekMode},
        reader::{DeserializationData, HkxBinaryReader},
        traits::HkxBinaryReadable,
    },
    packfile::{
        classname::HkxClassname,
        fixups::{Fixup, GlobalFixup, LocalFixup, VirtualFixup},
        header::{predicate::Predicate, Header},
        section::Section,
    },
    reflection::HkObject,
};
use std::{collections::HashMap, mem};

pub fn deserialize(bytes: &[u8]) -> Vec<HkObject> {
    let mut br = HkxBinaryReader::from_bytes(bytes);

    assert_eq!(br.peek::<u64>(), 0x57e0e057_10c0c010);

    br.step_in(0x11, SeekMode::Absolute);

    let endian = if br.read() {
        Endian::Little
    } else {
        Endian::Big
    };
    br.set_endian(endian);

    br.step_out();

    let header: Header = br.read();

    match header.predicate_array_size_plus_padding {
        0 => (),
        16 => {
            let _predicate: Predicate = br.read();
        }
        _ => panic!("unexpected predicate size!",),
    }

    let classnames: Section = br.read();
    let _types: Section = br.read();
    let data: Section = br.read();

    let classnames_offset: usize = classnames.absolute_data_start.try_into().unwrap();
    let classnames_size: usize = classnames.local_fixups_offset.try_into().unwrap();

    let data_offset: usize = data.absolute_data_start.try_into().unwrap();
    let lfu_offset: usize = data.local_fixups_offset.try_into().unwrap();
    let gfu_offset: usize = data.global_fixups_offset.try_into().unwrap();
    let vfu_offset: usize = data.virtual_fixups_offset.try_into().unwrap();
    let vfu_size: usize = data.exports_offset.try_into().unwrap();

    br.seek(classnames_offset, SeekMode::Absolute);
    let classnames = read_classnames(&mut br, classnames_offset, classnames_size);

    br.seek(data_offset + lfu_offset, SeekMode::Absolute);
    let local_fixups: HashMap<_, LocalFixup> = read_fixups(&mut br, gfu_offset - lfu_offset);

    br.seek(data_offset + gfu_offset, SeekMode::Absolute);
    let global_fixups: HashMap<_, GlobalFixup> = read_fixups(&mut br, vfu_offset - gfu_offset);

    br.seek(data_offset + vfu_offset, SeekMode::Absolute);
    let virtual_fixups: HashMap<_, VirtualFixup> = read_fixups(&mut br, vfu_size - vfu_offset);

    br.des = DeserializationData {
        layout_rules: header.layout_rules,
        data_offset: data_offset.try_into().unwrap(),
        local_fixups,
        global_fixups,
        virtual_fixups,
        classnames,
        objects: HashMap::new(),
    };

    br.seek(data_offset, SeekMode::Absolute);
    let object: HkObject = br.read();

    let mut should_continue = false;
    let mut next_file_bytes_range = 0..;
    if let HkObject::StaticCompoundInfo(ref sci) = object {
        let sci = sci.borrow();
        should_continue = true;
        next_file_bytes_range = sci.Offset.try_into().unwrap()..;
    }

    let mut ret = vec![object];

    if should_continue {
        ret.extend(deserialize(&br.bytes[next_file_bytes_range]))
    }

    ret
}

fn read_classnames(
    br: &mut HkxBinaryReader,
    classnames_offset: usize,
    classnames_size: usize,
) -> HashMap<u32, HkxClassname> {
    let mut classnames_map = HashMap::new();

    while br.peek::<u8>() != 0xff && br.position() != classnames_offset + classnames_size {
        let string_start = br.position() + 5 - classnames_offset;
        let class_name: HkxClassname = br.read();

        classnames_map.insert(string_start.try_into().unwrap(), class_name);
    }

    classnames_map
}

fn read_fixups<T>(br: &mut HkxBinaryReader, length: usize) -> HashMap<u32, T>
where
    T: Fixup + HkxBinaryReadable,
{
    let mut fixups = HashMap::new();
    for _ in 0..(length / mem::size_of::<T>()) {
        if br.peek::<u32>() == 0xFFFFFFFF {
            break;
        }
        let fixup: T = br.read();
        fixups.insert(fixup.get_src(), fixup);
    }

    fixups
}
