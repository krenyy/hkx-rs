use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hkpConvexVerticesConnectivity {
    pub vertexIndices: Array<u16>,
    pub numVerticesPerFace: Array<u8>,
}
