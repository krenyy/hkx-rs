use super::hkpLimitedHingeConstraintDataAtoms::hkpLimitedHingeConstraintDataAtoms;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConstraintData)]
pub struct hkpLimitedHingeConstraintData {
    #[pad(16)]
    pub atoms: hkpLimitedHingeConstraintDataAtoms,
}
