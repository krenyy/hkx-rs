use proc_macros::hk_class;

#[hk_class]
#[parent(hkcdStaticTreeCodec3Axis)]
pub struct hkcdStaticTreeCodec3Axis4 {
    pub data: u8,
}
