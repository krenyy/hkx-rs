use super::hclObjectSpaceDeformer::hclObjectSpaceDeformer;
use crate::binary::types::{Array, Matrix4};
use proc_macros::hk_class;

#[hk_class]
#[parent(hclOperator)]
#[descendants(hclObjectSpaceSkinPOperator)]
pub struct hclObjectSpaceSkinOperator {
    pub boneFromSkinMeshTransforms: Array<Matrix4>,
    pub transformSubset: Array<u16>,
    pub outputBufferIndex: u32,
    pub transformSetIndex: u32,
    pub objectSpaceDeformer: hclObjectSpaceDeformer,
}
