use super::{
    hclObjectSpaceDeformerEightBlendEntryBlock::hclObjectSpaceDeformerEightBlendEntryBlock,
    hclObjectSpaceDeformerFiveBlendEntryBlock::hclObjectSpaceDeformerFiveBlendEntryBlock,
    hclObjectSpaceDeformerFourBlendEntryBlock::hclObjectSpaceDeformerFourBlendEntryBlock,
    hclObjectSpaceDeformerOneBlendEntryBlock::hclObjectSpaceDeformerOneBlendEntryBlock,
    hclObjectSpaceDeformerSevenBlendEntryBlock::hclObjectSpaceDeformerSevenBlendEntryBlock,
    hclObjectSpaceDeformerSixBlendEntryBlock::hclObjectSpaceDeformerSixBlendEntryBlock,
    hclObjectSpaceDeformerThreeBlendEntryBlock::hclObjectSpaceDeformerThreeBlendEntryBlock,
    hclObjectSpaceDeformerTwoBlendEntryBlock::hclObjectSpaceDeformerTwoBlendEntryBlock,
};
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[pad(4)]
pub struct hclObjectSpaceDeformer {
    pub eightBlendEntries: Array<hclObjectSpaceDeformerEightBlendEntryBlock>,
    pub sevenBlendEntries: Array<hclObjectSpaceDeformerSevenBlendEntryBlock>,
    pub sixBlendEntries: Array<hclObjectSpaceDeformerSixBlendEntryBlock>,
    pub fiveBlendEntries: Array<hclObjectSpaceDeformerFiveBlendEntryBlock>,
    pub fourBlendEntries: Array<hclObjectSpaceDeformerFourBlendEntryBlock>,
    pub threeBlendEntries: Array<hclObjectSpaceDeformerThreeBlendEntryBlock>,
    pub twoBlendEntries: Array<hclObjectSpaceDeformerTwoBlendEntryBlock>,
    pub oneBlendEntries: Array<hclObjectSpaceDeformerOneBlendEntryBlock>,
    pub controlBytes: Array<u8>,
    pub startVertexIndex: u16,
    pub endVertexIndex: u16,
    pub batchSizeSpu: u16,
    pub partialWrite: bool,
}
