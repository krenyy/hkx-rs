use super::{
    hclBufferLayoutBufferElement::hclBufferLayoutBufferElement,
    hclBufferLayoutSlot::hclBufferLayoutSlot,
};
use crate::reflection::enums::TriangleFormat::TriangleFormat;
use proc_macros::hk_class;

#[hk_class]
pub struct hclBufferLayout {
    pub elementsLayout: [hclBufferLayoutBufferElement; 4],
    pub slots: [hclBufferLayoutSlot; 4],
    pub numSlots: u8,
    pub triangleFormat: TriangleFormat,
}
