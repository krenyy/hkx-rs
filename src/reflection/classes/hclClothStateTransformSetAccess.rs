use super::hclTransformSetUsage::hclTransformSetUsage;
use proc_macros::hk_class;

#[hk_class]
pub struct hclClothStateTransformSetAccess {
    pub transformSetIndex: u32,
    #[pad] // FIXME: why
    pub transformSetUsage: hclTransformSetUsage,
}
