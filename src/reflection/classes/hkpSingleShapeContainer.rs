use super::hkpShape::E_hkpShape;
use crate::binary::types::GlobalPointer;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpShapeContainer)]
pub struct hkpSingleShapeContainer {
    pub childShape: GlobalPointer<E_hkpShape>,
}
