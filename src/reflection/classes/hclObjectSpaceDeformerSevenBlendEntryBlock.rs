use proc_macros::hk_class;

#[hk_class]
pub struct hclObjectSpaceDeformerSevenBlendEntryBlock {
    pub vertexIndices: [u16; 16],
    // Rust doesn't implement Default for ([T; N] | N > 32)
    // pub boneIndices: [u16; 112],
    // pub boneWeights: [u16; 112],
    pub boneIndices: [[u16; 16]; 7],
    pub boneWeights: [[u16; 16]; 7],
}
