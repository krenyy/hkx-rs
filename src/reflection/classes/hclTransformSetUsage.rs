use super::hclTransformSetUsageTransformTracker::hclTransformSetUsageTransformTracker;
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
pub struct hclTransformSetUsage {
    pub perComponentFlags: [u8; 2],
    pub perComponentTransformTrackers: Array<hclTransformSetUsageTransformTracker>,
}
