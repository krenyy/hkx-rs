use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
#[descendants(hkpLimitedHingeConstraintData, hkpRagdollConstraintData)]
pub struct hkpConstraintData {
    pub userData: usize,
}
