use proc_macros::hk_class;

#[hk_class]
pub struct hclSimClothDataParticleData {
    pub mass: f32,
    pub invMass: f32,
    pub radius: f32,
    pub friction: f32,
}
