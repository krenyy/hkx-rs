use crate::binary::types::Vector4;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclShape)]
pub struct hclCapsuleShape {
    pub start: Vector4,
    pub end: Vector4,
    pub dir: Vector4,
    pub radius: f32,
    pub capLenSqrdInv: f32,
}
