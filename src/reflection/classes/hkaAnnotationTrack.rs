use super::hkaAnnotationTrackAnnotation::hkaAnnotationTrackAnnotation;
use crate::binary::types::{Array, StringPointer};
use proc_macros::hk_class;

#[hk_class]
pub struct hkaAnnotationTrack {
    pub trackName: StringPointer,
    pub annotations: Array<hkaAnnotationTrackAnnotation>,
}
