use crate::binary::types::f16;
use proc_macros::hk_class;

#[hk_class]
#[pad(16)]
pub struct hkaiStreamingSetGraphConnection {
    pub nodeIndex: i32,
    pub oppositeNodeIndex: i32,
    pub edgeData: u32,
    pub edgeCost: f16,
}
