use super::{
    hclObjectSpaceDeformerLocalBlockP::hclObjectSpaceDeformerLocalBlockP,
    hclObjectSpaceDeformerLocalBlockUnpackedP::hclObjectSpaceDeformerLocalBlockUnpackedP,
};
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclObjectSpaceSkinOperator)]
pub struct hclObjectSpaceSkinPOperator {
    pub localPs: Array<hclObjectSpaceDeformerLocalBlockP>,
    pub localUnpackedPs: Array<hclObjectSpaceDeformerLocalBlockUnpackedP>,
}
