use super::hkSphere::hkSphere;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclShape)]
pub struct hclSphereShape {
    pub sphere: hkSphere,
}
