use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
#[descendants(hkaiStaticTreeNavMeshQueryMediator)]
pub struct hkaiNavMeshQueryMediator {}
