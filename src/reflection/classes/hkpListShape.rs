use super::hkpListShapeChildInfo::hkpListShapeChildInfo;
use crate::binary::types::{Array, Vector4};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpShapeCollection)]
pub struct hkpListShape {
    pub childInfo: Array<hkpListShapeChildInfo>,
    pub flags: u16,
    pub numDisabledChildren: u16,
    pub aabbHalfExtents: Vector4,
    pub aabbCenter: Vector4,
    pub enabledChildren: [u32; 8],
}
