use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclOperator)]
#[pad(4)]
pub struct hclGatherAllVerticesOperator {
    pub vertexInputFromVertexOutput: Array<i16>,
    pub inputBufferIdx: u32,
    pub outputBufferIdx: u32,
    pub gatherNormals: bool,
    pub partialGather: bool,
}
