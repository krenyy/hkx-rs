use super::hkpConvexVerticesConnectivity::E_hkpConvexVerticesConnectivity;
use crate::binary::types::{Array, GlobalPointer, Matrix3, Vector4};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConvexShape)]
pub struct hkpConvexVerticesShape {
    pub aabbHalfExtents: Vector4,
    pub aabbCenter: Vector4,
    pub rotatedVertices: Array<Matrix3>,
    pub numVertices: i32,
    #[assert(false)]
    _useSpuBuffer: bool,
    #[pad(4)]
    pub planeEquations: Array<Vector4>,
    pub connectivity: GlobalPointer<E_hkpConvexVerticesConnectivity>,
}
