use crate::binary::types::VoidPointer;
use proc_macros::hk_class;

#[hk_class]
pub struct hkpEntitySpuCollisionCallback {
    _util: VoidPointer,
    _capacity: u16,
    pub eventFilter: u8,
    pub userFilter: u8,
}
