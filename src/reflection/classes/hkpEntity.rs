use super::{
    hkLocalFrame::E_hkLocalFrame, hkpConstraintInstance::E_hkpConstraintInstance,
    hkpEntityExtendedListeners::E_hkpEntityExtendedListeners,
    hkpEntitySmallArraySerializeOverrideType::hkpEntitySmallArraySerializeOverrideType,
    hkpEntitySpuCollisionCallback::hkpEntitySpuCollisionCallback, hkpMaterial::hkpMaterial,
    hkpMaxSizeMotion::hkpMaxSizeMotion,
};
use crate::binary::types::{Array, GlobalPointer, VoidPointer};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpWorldObject)]
#[descendants(hkpRigidBody)]
pub struct hkpEntity {
    pub material: hkpMaterial,
    _limitContactImpulseUtilAndFlag: VoidPointer,
    pub damageMultiplier: f32,
    _breakableBody: VoidPointer,
    #[assert(0)]
    _solverData: u32,
    pub storageIndex: u16,
    pub contactPointCallbackDelay: u16,
    _constraintsMaster: hkpEntitySmallArraySerializeOverrideType,
    _constraintsSlave: Array<GlobalPointer<E_hkpConstraintInstance>>,
    _constraintRuntime: Array<u8>,
    _simulationIsland: VoidPointer,
    pub autoRemoveLevel: i8,
    pub numShapeKeysInContactPointProperties: u8,
    pub responseModifierFlags: u8,
    #[pad(4)]
    pub uid: u32,
    pub spuCollisionCallback: hkpEntitySpuCollisionCallback,
    #[pad(16)]
    pub motion: hkpMaxSizeMotion,
    #[pad(16)]
    _contactListeners: hkpEntitySmallArraySerializeOverrideType,
    _actions: hkpEntitySmallArraySerializeOverrideType,
    pub localFrame: GlobalPointer<E_hkLocalFrame>,
    _extendedListeners: GlobalPointer<E_hkpEntityExtendedListeners>,
    pub npData: u32,
}
