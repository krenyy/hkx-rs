use proc_macros::hk_class;

#[hk_class]
pub struct hclSimpleMeshBoneDeformOperatorTriangleBonePair {
    pub boneOffset: u16,
    pub triangleOffset: u16,
}
