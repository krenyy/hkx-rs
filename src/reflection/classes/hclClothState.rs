use super::{
    hclClothStateBufferAccess::hclClothStateBufferAccess,
    hclClothStateTransformSetAccess::hclClothStateTransformSetAccess,
};
use crate::binary::types::{Array, StringPointer};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hclClothState {
    pub name: StringPointer,
    pub operators: Array<u32>,
    pub usedBuffers: Array<hclClothStateBufferAccess>,
    pub usedTransformSets: Array<hclClothStateTransformSetAccess>,
    pub usedSimCloths: Array<u32>,
}
