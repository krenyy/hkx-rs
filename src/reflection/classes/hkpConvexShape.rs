use proc_macros::hk_class;

#[hk_class]
#[parent(hkpSphereRepShape)]
#[descendants(
    hkpBoxShape,
    hkpCapsuleShape,
    hkpConvexTransformShape,
    hkpConvexTransformShapeBase,
    hkpConvexTranslateShape,
    hkpConvexVerticesShape,
    hkpCylinderShape,
    hkpSphereShape
)]
pub struct hkpConvexShape {
    pub radius: f32,
}
