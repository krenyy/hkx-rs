use super::{
    hkAabb::hkAabb, hkaiNavMeshEdge::hkaiNavMeshEdge, hkaiNavMeshFace::hkaiNavMeshFace,
    hkaiStreamingSet::hkaiStreamingSet,
};
use crate::binary::types::{Array, Vector4};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hkaiNavMesh {
    pub faces: Array<hkaiNavMeshFace>,
    pub edges: Array<hkaiNavMeshEdge>,
    pub vertices: Array<Vector4>,
    pub streamingSets: Array<hkaiStreamingSet>,
    pub faceData: Array<i32>,
    pub edgeData: Array<i32>,
    pub faceDataStriding: i32,
    pub edgeDataStriding: i32,
    pub flags: u8,
    pub aabb: hkAabb,
    pub erosionRadius: f32,
    pub userData: usize,
}
