use crate::binary::types::Vector4;
use proc_macros::hk_class;

#[hk_class]
#[pad(4)]
pub struct hclSimClothDataOverridableSimulationInfo {
    pub gravity: Vector4,
    pub globalDampingPerSecond: f32,
    pub collisionTolerance: f32,
    pub subSteps: u32,
    pub pinchDetectionEnabled: bool,
    pub landscapeCollisionEnabled: bool,
    pub transferMotionEnabled: bool,
}
