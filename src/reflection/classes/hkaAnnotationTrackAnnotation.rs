use crate::binary::types::StringPointer;
use proc_macros::hk_class;

#[hk_class]
pub struct hkaAnnotationTrackAnnotation {
    pub time: f32,
    pub text: StringPointer,
}
