use crate::binary::types::Vector4;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclShape)]
pub struct hclPlaneShape {
    pub planeEquation: Vector4,
}
