use proc_macros::hk_class;

#[hk_class]
#[parent(hkcdStaticTreeDynamicStoragehkcdStaticTreeCodec3Axis6)]
#[descendants(
    hkcdStaticTreeDefaultTreeStorage6,
    hkcdStaticTreeTreehkcdStaticTreeDynamicStorage6
)]
pub struct hkcdStaticTreeDynamicStorage6 {}
