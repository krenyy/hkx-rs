use super::{
    hkpConstraintData::E_hkpConstraintData,
    hkpConstraintInstanceSmallArraySerializeOverrideType::hkpConstraintInstanceSmallArraySerializeOverrideType,
    hkpEntity::E_hkpEntity, hkpModifierConstraintAtom::E_hkpModifierConstraintAtom,
};
use crate::{
    binary::types::{GlobalPointer, StringPointer, VoidPointer},
    reflection::enums::{
        ConstraintPriority::ConstraintPriority, OnDestructionRemapInfo::OnDestructionRemapInfo,
    },
};
use proc_macros::hk_class;

#[hk_class]
#[signature = 0xda4ce91e]
#[parent(hkReferencedObject)]
pub struct hkpConstraintInstance {
    _owner: VoidPointer,
    pub data: GlobalPointer<E_hkpConstraintData>,
    pub constraintModifiers: GlobalPointer<E_hkpModifierConstraintAtom>,
    pub entities: [GlobalPointer<E_hkpEntity>; 2],
    pub priority: ConstraintPriority,
    pub wantRuntime: bool,
    pub destructionRemapInfo: OnDestructionRemapInfo,
    _listeners: hkpConstraintInstanceSmallArraySerializeOverrideType,
    pub name: StringPointer,
    pub userData: usize,
    _internal: VoidPointer,
    #[assert(0)]
    _uid: u32,
}
