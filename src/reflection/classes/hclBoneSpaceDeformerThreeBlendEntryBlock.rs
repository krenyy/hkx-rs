use proc_macros::hk_class;

#[hk_class]
pub struct hclBoneSpaceDeformerThreeBlendEntryBlock {
    pub vertexIndices: [u16; 5],
    pub boneIndices: [u16; 15],
    #[assert(0, 0, 0, 0, 0, 0, 0, 0)]
    _padding: [u8; 8],
}
