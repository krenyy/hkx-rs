use super::{
    hkaAnimation::E_hkaAnimation, hkaAnimationBinding::E_hkaAnimationBinding,
    hkaBoneAttachment::E_hkaBoneAttachment, hkaMeshBinding::E_hkaMeshBinding,
    hkaSkeleton::E_hkaSkeleton,
};
use crate::binary::types::{Array, GlobalPointer};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hkaAnimationContainer {
    pub skeletons: Array<GlobalPointer<E_hkaSkeleton>>,
    pub animations: Array<GlobalPointer<E_hkaAnimation>>,
    pub bindings: Array<GlobalPointer<E_hkaAnimationBinding>>,
    pub attachments: Array<GlobalPointer<E_hkaBoneAttachment>>,
    pub skins: Array<GlobalPointer<E_hkaMeshBinding>>,
}
