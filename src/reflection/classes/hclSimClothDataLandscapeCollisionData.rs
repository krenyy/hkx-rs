use proc_macros::hk_class;

#[hk_class]
pub struct hclSimClothDataLandscapeCollisionData {
    pub landscapeRadius: f32,
    pub enableStuckParticleDetection: bool,
    #[pad(4)]
    pub stuckParticlesStretchFactorSq: f32,
    pub pinchDetectionEnabled: bool,
    pub pinchDetectionPriority: i8,
    #[pad(4)]
    pub pinchDetectionRadius: f32,
}
