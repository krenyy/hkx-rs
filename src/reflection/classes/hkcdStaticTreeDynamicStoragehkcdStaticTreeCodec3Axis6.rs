use super::hkcdStaticTreeCodec3Axis6::hkcdStaticTreeCodec3Axis6;
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[descendants(
    hkcdStaticTreeDefaultTreeStorage6,
    hkcdStaticTreeDynamicStorage6,
    hkcdStaticTreeTreehkcdStaticTreeDynamicStorage6
)]
#[pad(16)]
pub struct hkcdStaticTreeDynamicStoragehkcdStaticTreeCodec3Axis6 {
    pub nodes: Array<hkcdStaticTreeCodec3Axis6>,
}
