use proc_macros::hk_class;

#[hk_class]
pub struct hclObjectSpaceDeformerTwoBlendEntryBlock {
    pub vertexIndices: [u16; 16],
    pub boneIndices: [u16; 32],
    pub boneWeights: [u8; 32],
}
