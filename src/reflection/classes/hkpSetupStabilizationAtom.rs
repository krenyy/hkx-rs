use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConstraintAtom)]
pub struct hkpSetupStabilizationAtom {
    pub enabled: bool,
    #[assert(0)]
    _padding: u8, // TODO: [u8; 1]
    pub maxLinImpulse: f32,
    pub maxAngImpulse: f32,
    pub maxAngle: f32,
}
