use super::hkpShape::E_hkpShape;
use crate::binary::types::{GlobalPointer, QsTransform};
use proc_macros::hk_class;

#[hk_class]
#[pad(16)] // FIXME: not sure
pub struct hkpStaticCompoundShapeInstance {
    pub transform: QsTransform,
    pub shape: GlobalPointer<E_hkpShape>,
    pub filterInfo: u32,
    pub childFilterInfoMask: u32,
    pub userData: usize,
}
