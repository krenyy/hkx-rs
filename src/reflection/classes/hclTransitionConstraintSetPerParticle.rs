use proc_macros::hk_class;

#[hk_class]
pub struct hclTransitionConstraintSetPerParticle {
    pub particleIndex: u16,
    pub referenceVertex: u16,
    pub toAnimDelay: f32,
    pub toSimDelay: f32,
    pub toSimMaxDistance: f32,
}
