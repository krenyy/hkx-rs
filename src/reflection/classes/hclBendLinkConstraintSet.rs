use super::hclBendLinkConstraintSetLink::hclBendLinkConstraintSetLink;
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclConstraintSet)]
pub struct hclBendLinkConstraintSet {
    pub links: Array<hclBendLinkConstraintSetLink>,
}
