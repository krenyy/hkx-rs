use super::{
    hkaSkeleton::E_hkaSkeleton,
    hkaSkeletonMapperDataChainMapping::hkaSkeletonMapperDataChainMapping,
    hkaSkeletonMapperDataPartitionMappingRange::hkaSkeletonMapperDataPartitionMappingRange,
    hkaSkeletonMapperDataSimpleMapping::hkaSkeletonMapperDataSimpleMapping,
};
use crate::{
    binary::types::{Array, GlobalPointer, QsTransform},
    reflection::enums::MappingType::MappingType,
};
use proc_macros::hk_class;

#[hk_class]
pub struct hkaSkeletonMapperData {
    pub skeletonA: GlobalPointer<E_hkaSkeleton>,
    pub skeletonB: GlobalPointer<E_hkaSkeleton>,
    pub partitionMap: Array<i16>,
    pub simpleMappingPartitionRanges: Array<hkaSkeletonMapperDataPartitionMappingRange>,
    pub chainMappingPartitionRanges: Array<hkaSkeletonMapperDataPartitionMappingRange>,
    pub simpleMappings: Array<hkaSkeletonMapperDataSimpleMapping>,
    pub chainMappings: Array<hkaSkeletonMapperDataChainMapping>,
    pub unmappedBones: Array<i16>,
    pub extractedMotionMapping: QsTransform,
    pub keepUnmappedLocal: bool,
    #[pad(4)]
    pub mappingType: MappingType,
}
