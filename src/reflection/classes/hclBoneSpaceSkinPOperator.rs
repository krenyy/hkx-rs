use super::{
    hclBoneSpaceDeformerLocalBlockP::hclBoneSpaceDeformerLocalBlockP,
    hclBoneSpaceDeformerLocalBlockUnpackedP::hclBoneSpaceDeformerLocalBlockUnpackedP,
};
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclBoneSpaceSkinOperator)]
pub struct hclBoneSpaceSkinPOperator {
    pub localPs: Array<hclBoneSpaceDeformerLocalBlockP>,
    pub localUnpackedPs: Array<hclBoneSpaceDeformerLocalBlockUnpackedP>,
}
