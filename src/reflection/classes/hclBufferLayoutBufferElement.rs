use crate::reflection::enums::VectorConversion::VectorConversion;
use proc_macros::hk_class;

#[hk_class]
pub struct hclBufferLayoutBufferElement {
    pub vectorConversion: VectorConversion,
    pub vectorSize: u8,
    pub slotId: u8,
    pub slotStart: u8,
}
