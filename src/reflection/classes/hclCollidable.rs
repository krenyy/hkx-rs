use super::hclShape::E_hclShape;
use crate::binary::types::{GlobalPointer, StringPointer, Transform, Vector4};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hclCollidable {
    pub name: StringPointer,
    pub transform: Transform,
    pub linearVelocity: Vector4,
    pub angularVelocity: Vector4,
    pub pinchDetectionEnabled: bool,
    pub pinchDetectionPriority: u8,
    #[pad(4)]
    pub pinchDetectionRadius: f32,
    pub shape: GlobalPointer<E_hclShape>,
}
