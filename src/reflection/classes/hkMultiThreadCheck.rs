use proc_macros::hk_class;

#[hk_class]
pub struct hkMultiThreadCheck {
    #[assert(0)]
    _threadId: u32,
    #[assert(0)]
    _stackTraceId: i32,
    #[assert(0)]
    _markCount: u16,
    #[assert(0)]
    _markBitStack: u16,
}
