use super::{
    hkMultiThreadCheck::hkMultiThreadCheck, hkSimpleProperty::hkSimpleProperty,
    hkpLinkedCollidable::hkpLinkedCollidable,
};
use crate::binary::types::{Array, StringPointer, VoidPointer};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
#[descendants(hkpEntity, hkpRigidBody)]
pub struct hkpWorldObject {
    _world: VoidPointer,
    pub userData: usize,
    pub collidable: hkpLinkedCollidable,
    pub multiThreadCheck: hkMultiThreadCheck,
    pub name: StringPointer,
    pub properties: Array<hkSimpleProperty>,
}
