use super::{
    hkpCollidableBoundingVolumeData::hkpCollidableBoundingVolumeData,
    hkpTypedBroadPhaseHandle::hkpTypedBroadPhaseHandle,
};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpCdBody)]
#[descendants(hkpLinkedCollidable)]
pub struct hkpCollidable {
    #[assert(0)]
    _ownerOffset: u8,
    pub forceCollideOntoPpu: u8,
    #[assert(0)]
    _shapeSizeOnSpu: u16,
    pub broadPhaseHandle: hkpTypedBroadPhaseHandle,
    _boundingVolumeData: hkpCollidableBoundingVolumeData,
    pub allowedPenetrationDepth: f32,
}
