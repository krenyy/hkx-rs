use super::hkUFloat8::hkUFloat8;
use crate::reflection::enums::SolvingMethod::SolvingMethod;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConstraintAtom)]
pub struct hkpBallSocketConstraintAtom {
    pub solvingMethod: SolvingMethod,
    pub bodiesToNotify: u8,
    pub velocityStabilizationFactor: hkUFloat8,
    pub enableLinearImpulseLimit: bool,
    #[pad(4)]
    pub breachImpulse: f32,
    pub inertiaStabilizationFactor: f32,
}
