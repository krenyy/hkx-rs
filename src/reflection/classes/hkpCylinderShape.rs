use crate::binary::types::Vector4;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConvexShape)]
pub struct hkpCylinderShape {
    pub cylRadius: f32,
    pub cylBaseRadiusFactorForHeightFieldCollisions: f32,
    pub vertexA: Vector4,
    pub vertexB: Vector4,
    pub perpendicular1: Vector4,
    pub perpendicular2: Vector4,
}
