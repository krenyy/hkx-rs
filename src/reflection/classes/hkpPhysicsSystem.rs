use super::{
    hkReferencedObject::E_hkReferencedObject, hkpConstraintInstance::E_hkpConstraintInstance,
    hkpRigidBody::E_hkpRigidBody,
};
use crate::binary::types::{Array, GlobalPointer, StringPointer};
use proc_macros::hk_class;

#[hk_class]
#[signature = 0xb3cc6e64]
#[parent(hkReferencedObject)]
pub struct hkpPhysicsSystem {
    pub rigidBodies: Array<GlobalPointer<E_hkpRigidBody>>,
    pub constraints: Array<GlobalPointer<E_hkpConstraintInstance>>,
    pub actions: Array<GlobalPointer<E_hkReferencedObject>>, // TODO: E_hkpAction
    pub phantoms: Array<GlobalPointer<E_hkReferencedObject>>, // TODO: E_hkpPhantom
    pub name: StringPointer,
    pub userData: usize,
    pub active: bool,
}
