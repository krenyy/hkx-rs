use proc_macros::hk_class;

#[hk_class]
pub struct ActorInfo {
    pub HashId: u32,
    pub SRTHash: i32,
    pub ShapeInfoStart: i32,
    pub ShapeInfoEnd: i32,
}
