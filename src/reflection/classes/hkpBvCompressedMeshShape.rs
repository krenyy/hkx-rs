use super::hkpBvCompressedMeshShapeTree::hkpBvCompressedMeshShapeTree;
use crate::{
    binary::types::{Array, StringPointer, VoidPointer},
    reflection::enums::WeldingType::WeldingType,
};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpBvTreeShape)]
pub struct hkpBvCompressedMeshShape {
    _discard0: VoidPointer, // FIXME: honestly, no idea what's going on here
    pub convexRadius: f32,
    pub weldingType: WeldingType,
    pub hasPerPrimitiveCollisionFilterInfo: bool,
    pub hasPerPrimitiveUserData: bool,
    #[pad(4)]
    pub collisionFilterInfoPalette: Array<u32>,
    pub userDataPalette: Array<u32>,
    pub userStringPalette: Array<StringPointer>,
    #[pad(16)]
    pub tree: hkpBvCompressedMeshShapeTree,
}
