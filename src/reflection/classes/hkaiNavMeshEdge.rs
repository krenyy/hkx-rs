use crate::{binary::types::f16, reflection::enums::EdgeFlagBits::EdgeFlagBits};
use proc_macros::hk_class;

#[hk_class]
pub struct hkaiNavMeshEdge {
    pub a: i32,
    pub b: i32,
    pub oppositeEdge: u32,
    pub oppositeFace: u32,
    pub flags: EdgeFlagBits,
    #[assert(0)]
    _paddingByte: u8,
    pub userEdgeCost: f16,
}
