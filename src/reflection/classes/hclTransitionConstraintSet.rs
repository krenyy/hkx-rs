use super::hclTransitionConstraintSetPerParticle::hclTransitionConstraintSetPerParticle;
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclConstraintSet)]
pub struct hclTransitionConstraintSet {
    pub perParticleData: Array<hclTransitionConstraintSetPerParticle>,
    pub toAnimPeriod: f32,
    pub toAnimPlusDelayPeriod: f32,
    pub toSimPeriod: f32,
    pub toSimPlusDelayPeriod: f32,
    pub referenceMeshBufferIdx: u32,
}
