use proc_macros::hk_class;

#[hk_class]
#[parent(hkcdStaticTreeCodec3Axis)]
pub struct hkcdStaticTreeCodec3Axis5 {
    pub hiData: u8,
    pub loData: u8,
}
