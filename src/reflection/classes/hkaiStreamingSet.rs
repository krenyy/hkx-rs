use super::{
    hkaiStreamingSetGraphConnection::hkaiStreamingSetGraphConnection,
    hkaiStreamingSetNavMeshConnection::hkaiStreamingSetNavMeshConnection,
    hkaiStreamingSetVolumeConnection::hkaiStreamingSetVolumeConnection,
};
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
pub struct hkaiStreamingSet {
    pub thisUid: u32,
    pub oppositeUid: u32,
    pub meshConnections: Array<hkaiStreamingSetNavMeshConnection>,
    pub graphConnections: Array<hkaiStreamingSetGraphConnection>,
    pub volumeConnections: Array<hkaiStreamingSetVolumeConnection>,
}
