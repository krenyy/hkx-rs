use proc_macros::hk_class;

#[hk_class]
pub struct hclMoveParticlesOperatorVertexParticlePair {
    pub vertexIndex: u16,
    pub particleIndex: u16,
}
