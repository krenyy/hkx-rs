use proc_macros::hk_class;

#[hk_class]
pub struct hkaiStreamingSetVolumeConnection {
    pub cellIndex: i32,
    pub oppositecellIndex: i32,
}
