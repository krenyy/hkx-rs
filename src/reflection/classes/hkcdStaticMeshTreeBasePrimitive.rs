use proc_macros::hk_class;

#[hk_class]
pub struct hkcdStaticMeshTreeBasePrimitive {
    pub indices: [u8; 4],
}
