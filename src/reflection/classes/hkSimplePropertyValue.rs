use proc_macros::hk_class;

#[hk_class]
pub struct hkSimplePropertyValue {
    pub data: u64,
}
