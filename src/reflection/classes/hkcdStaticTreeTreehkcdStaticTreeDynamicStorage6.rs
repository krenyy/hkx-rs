use super::hkAabb::hkAabb;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkcdStaticTreeDynamicStorage6)]
#[descendants(hkcdStaticTreeDefaultTreeStorage6)]
pub struct hkcdStaticTreeTreehkcdStaticTreeDynamicStorage6 {
    pub domain: hkAabb,
}
