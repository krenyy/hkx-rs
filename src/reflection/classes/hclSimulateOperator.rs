use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclOperator)]
#[pad(4)]
pub struct hclSimulateOperator {
    pub simClothIndex: u32,
    pub subSteps: u32,
    pub numberOfSolveIterations: i32,
    pub constraintExecution: Array<i32>,
    pub adaptConstraintStiffness: bool,
}
