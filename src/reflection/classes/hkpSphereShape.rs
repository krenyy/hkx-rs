use proc_macros::hk_class;

#[hk_class]
#[signature = 0x37aa32c9]
#[parent(hkpConvexShape)]
pub struct hkpSphereShape {
    #[assert(0, 0, 0)]
    _pad16: [u32; 3],
}
