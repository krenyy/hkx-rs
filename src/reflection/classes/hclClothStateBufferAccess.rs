use super::hclBufferUsage::hclBufferUsage;
use proc_macros::hk_class;

#[hk_class]
pub struct hclClothStateBufferAccess {
    pub bufferIndex: u32,
    pub bufferUsage: hclBufferUsage,
    #[pad(4)]
    pub shadowBufferIndex: u32,
}
