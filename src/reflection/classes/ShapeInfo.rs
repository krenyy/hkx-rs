use proc_macros::hk_class;

#[hk_class]
#[pad(4)]
pub struct ShapeInfo {
    pub ActorInfoIndex: i32,
    pub InstanceId: i32,
    pub BodyGroup: i8,
    pub BodyLayerType: u8,
}
