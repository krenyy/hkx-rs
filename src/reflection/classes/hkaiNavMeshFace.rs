use proc_macros::hk_class;

#[hk_class]
pub struct hkaiNavMeshFace {
    pub startEdgeIndex: i32,
    pub startUserEdgeIndex: i32,
    pub numEdges: i16,
    pub numUserEdges: i16,
    pub clusterIndex: i16,
    pub padding: i16,
}
