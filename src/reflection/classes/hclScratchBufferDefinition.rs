use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclBufferDefinition)]
#[pad(4)]
pub struct hclScratchBufferDefinition {
    pub triangleIndices: Array<u16>,
    pub storeNormals: bool,
    pub storeTangentsAndBiTangents: bool,
}
