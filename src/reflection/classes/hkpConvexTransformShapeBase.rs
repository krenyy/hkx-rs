use super::hkpSingleShapeContainer::hkpSingleShapeContainer;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConvexShape)]
#[descendants(hkpConvexTransformShape, hkpConvexTranslateShape)]
pub struct hkpConvexTransformShapeBase {
    pub childShape: hkpSingleShapeContainer,
    #[assert(0)]
    _childShapeSizeForSpu: i32,
}
