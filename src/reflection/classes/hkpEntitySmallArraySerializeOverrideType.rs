use crate::binary::types::VoidPointer;
use proc_macros::hk_class;

#[hk_class]
#[pad(8)]
pub struct hkpEntitySmallArraySerializeOverrideType {
    _data: VoidPointer,
    pub size: u16,
    pub capacityAndFlags: u16,
}
