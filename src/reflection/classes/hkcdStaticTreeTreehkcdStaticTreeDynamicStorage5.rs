use super::hkAabb::hkAabb;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkcdStaticTreeDynamicStorage5)]
#[descendants(
    hkcdStaticMeshTreeBase,
    hkcdStaticMeshTreehkcdStaticMeshTreeCommonConfigunsignedintunsignedlonglong1121hkpBvCompressedMeshShapeTreeDataRun,
    hkpBvCompressedMeshShapeTree
)]
pub struct hkcdStaticTreeTreehkcdStaticTreeDynamicStorage5 {
    pub domain: hkAabb,
}
