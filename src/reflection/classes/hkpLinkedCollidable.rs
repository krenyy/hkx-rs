use crate::binary::types::VoidArray;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpCollidable)]
pub struct hkpLinkedCollidable {
    _collisionEntries: VoidArray,
}
