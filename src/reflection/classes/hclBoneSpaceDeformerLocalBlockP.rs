use crate::binary::types::Vector4;
use proc_macros::hk_class;

#[hk_class]
pub struct hclBoneSpaceDeformerLocalBlockP {
    pub localPosition: [Vector4; 16],
}
