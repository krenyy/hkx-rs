use proc_macros::hk_class;

#[hk_class]
pub struct hclBendLinkConstraintSetLink {
    pub particleA: u16,
    pub particleB: u16,
    pub bendMinLength: f32,
    pub stretchMaxLength: f32,
    pub bendStiffness: f32,
    pub stretchStiffness: f32,
}
