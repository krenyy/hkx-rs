use super::{
    hkaBone::hkaBone, hkaSkeletonLocalFrameOnBone::hkaSkeletonLocalFrameOnBone,
    hkaSkeletonPartition::hkaSkeletonPartition,
};
use crate::binary::types::{Array, QsTransform, StringPointer};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hkaSkeleton {
    pub name: StringPointer,
    pub parentIndices: Array<i16>,
    pub bones: Array<hkaBone>,
    pub referencePose: Array<QsTransform>,
    pub referenceFloats: Array<f32>,
    pub floatSlots: Array<StringPointer>,
    pub localFrames: Array<hkaSkeletonLocalFrameOnBone>,
    pub partitions: Array<hkaSkeletonPartition>,
}
