use super::{
    hclVolumeConstraintApplyData::hclVolumeConstraintApplyData,
    hclVolumeConstraintFrameData::hclVolumeConstraintFrameData,
};
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclConstraintSet)]
pub struct hclVolumeConstraint {
    pub frameDatas: Array<hclVolumeConstraintFrameData>,
    pub applyDatas: Array<hclVolumeConstraintApplyData>,
}
