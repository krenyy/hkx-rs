use crate::binary::types::Vector4;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConvexShape)]
pub struct hkpBoxShape {
    pub halfExtents: Vector4,
}
