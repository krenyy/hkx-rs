use super::{
    hkaSkeleton::E_hkaSkeleton, hkpConstraintInstance::E_hkpConstraintInstance,
    hkpRigidBody::E_hkpRigidBody,
};
use crate::binary::types::{Array, GlobalPointer};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hkaRagdollInstance {
    pub rigidBodies: Array<GlobalPointer<E_hkpRigidBody>>,
    pub constraints: Array<GlobalPointer<E_hkpConstraintInstance>>,
    pub boneToRigidBodyMap: Array<i32>,
    pub skeleton: GlobalPointer<E_hkaSkeleton>,
}
