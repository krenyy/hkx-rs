use super::{hkMotionState::hkMotionState, hkpMaxSizeMotion::E_hkpMaxSizeMotion};
use crate::{
    binary::types::{f16, GlobalPointer, Vector4},
    reflection::enums::MotionType::MotionType,
};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
#[descendants(hkpKeyframedRigidMotion, hkpMaxSizeMotion)]
pub struct hkpMotion {
    pub r#type: MotionType,
    pub deactivationIntegrateCounter: u8,
    pub deactivationNumInactiveFrames: [u16; 2],
    pub motionState: hkMotionState,
    pub inertiaAndMassInv: Vector4,
    pub linearVelocity: Vector4,
    pub angularVelocity: Vector4,
    pub deactivationRefPosition: [Vector4; 2],
    pub deactivationRefOrientation: [u32; 2],
    pub savedMotion: GlobalPointer<E_hkpMaxSizeMotion>,
    pub savedQualityTypeIndex: u16,
    pub gravityFactor: f16,
}
