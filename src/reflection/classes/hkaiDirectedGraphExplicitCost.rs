use super::{
    hkaiDirectedGraphExplicitCostEdge::hkaiDirectedGraphExplicitCostEdge,
    hkaiDirectedGraphExplicitCostNode::hkaiDirectedGraphExplicitCostNode,
    hkaiStreamingSet::hkaiStreamingSet,
};
use crate::binary::types::{Array, Vector4};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hkaiDirectedGraphExplicitCost {
    pub positions: Array<Vector4>,
    pub nodes: Array<hkaiDirectedGraphExplicitCostNode>,
    pub edges: Array<hkaiDirectedGraphExplicitCostEdge>,
    pub nodeData: Array<u32>,
    pub edgeData: Array<u32>,
    pub nodeDataStriding: i32,
    pub edgeDataStriding: i32,
    pub streamingSets: Array<hkaiStreamingSet>,
}
