use super::hkBitField::hkBitField;
use proc_macros::hk_class;

#[hk_class]
pub struct hclTransformSetUsageTransformTracker {
    pub read: hkBitField,
    pub readBeforeWrite: hkBitField,
    pub written: hkBitField,
}
