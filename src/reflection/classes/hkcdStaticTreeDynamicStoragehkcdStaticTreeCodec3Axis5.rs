use super::hkcdStaticTreeCodec3Axis5::hkcdStaticTreeCodec3Axis5;
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[descendants(
    hkcdStaticMeshTreeBase,
    hkcdStaticMeshTreehkcdStaticMeshTreeCommonConfigunsignedintunsignedlonglong1121hkpBvCompressedMeshShapeTreeDataRun,
    hkcdStaticTreeDynamicStorage5,
    hkcdStaticTreeTreehkcdStaticTreeDynamicStorage5,
    hkpBvCompressedMeshShapeTree

    )]
#[pad(16)]
pub struct hkcdStaticTreeDynamicStoragehkcdStaticTreeCodec3Axis5 {
    pub nodes: Array<hkcdStaticTreeCodec3Axis5>,
}
