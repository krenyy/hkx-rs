use super::hkpConstraintMotor::E_hkpConstraintMotor;
use crate::binary::types::GlobalPointer;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConstraintAtom)]
pub struct hkpAngMotorConstraintAtom {
    pub isEnabled: bool,
    pub motorAxis: u8,
    #[assert(0)]
    _initializedOffset: i16,
    #[assert(0)]
    _previousTargetAngleOffset: i16,
    pub motor: GlobalPointer<E_hkpConstraintMotor>,
    #[pad(16)]
    pub targetAngle: f32,
    #[assert(0)]
    _correspondingAngLimitSolverResultOffset: i16,
    #[assert(0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
    _padding: [u8; 10],
}
