use crate::binary::types::Vector4;
use proc_macros::hk_class;

#[hk_class]
pub struct hclVolumeConstraintApplyData {
    pub frameVector: Vector4,
    pub particleIndex: u16,
    #[pad(2)]
    pub stiffness: f32,
}
