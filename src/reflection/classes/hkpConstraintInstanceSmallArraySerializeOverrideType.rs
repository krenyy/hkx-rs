use crate::binary::types::VoidPointer;
use proc_macros::hk_class;

#[hk_class]
pub struct hkpConstraintInstanceSmallArraySerializeOverrideType {
    _data: VoidPointer,
    pub size: u16,
    pub capacityAndFlags: u16,
}
