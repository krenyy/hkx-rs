use proc_macros::hk_class;

#[hk_class]
pub struct hclBoneSpaceDeformerFourBlendEntryBlock {
    pub vertexIndices: [u16; 4],
    pub boneIndices: [u16; 16],
    #[assert(0, 0, 0, 0, 0, 0, 0, 0)]
    _padding: [u8; 8],
}
