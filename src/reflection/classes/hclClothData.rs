use super::{
    hclAction::E_hclAction, hclBufferDefinition::E_hclBufferDefinition,
    hclClothState::E_hclClothState, hclOperator::E_hclOperator, hclSimClothData::E_hclSimClothData,
    hclTransformSetDefinition::E_hclTransformSetDefinition,
};
use crate::{
    binary::types::{Array, GlobalPointer, StringPointer},
    reflection::enums::Platform::Platform,
};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hclClothData {
    pub name: StringPointer,
    pub simClothDatas: Array<GlobalPointer<E_hclSimClothData>>,
    pub bufferDefinitions: Array<GlobalPointer<E_hclBufferDefinition>>,
    pub transformSetDefinitions: Array<GlobalPointer<E_hclTransformSetDefinition>>,
    pub operators: Array<GlobalPointer<E_hclOperator>>,
    pub clothStateDatas: Array<GlobalPointer<E_hclClothState>>,
    pub actions: Array<GlobalPointer<E_hclAction>>,
    pub targetPlatform: Platform,
}
