use crate::binary::types::Vector4;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConvexTransformShapeBase)]
pub struct hkpConvexTranslateShape {
    pub translation: Vector4,
}
