use crate::binary::types::StringPointer;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hclTransformSetDefinition {
    pub name: StringPointer,
    pub r#type: i32,
    pub numTransforms: u32,
}
