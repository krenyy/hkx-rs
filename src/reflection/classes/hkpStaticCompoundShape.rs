use super::{
    hkcdStaticTreeDefaultTreeStorage6::hkcdStaticTreeDefaultTreeStorage6,
    hkpShapeKeyTable::hkpShapeKeyTable,
    hkpStaticCompoundShapeInstance::hkpStaticCompoundShapeInstance,
};
use crate::binary::types::{Array, VoidPointer};
use proc_macros::hk_class;

#[hk_class]
#[signature = 0xd029071e]
#[parent(hkpBvTreeShape)]
pub struct hkpStaticCompoundShape {
    _discard0: VoidPointer, // FIXME: no idea
    pub numBitsForChildShapeKey: i8,
    #[assert(0)]
    _referencePolicy: i8,
    #[pad(4)]
    #[assert(0)]
    _childShapeKeyMask: u32,
    pub instances: Array<hkpStaticCompoundShapeInstance>,
    pub instanceExtraInfos: Array<u16>,
    pub disabledLargeShapeKeyTable: hkpShapeKeyTable,
    pub tree: hkcdStaticTreeDefaultTreeStorage6,
}
