use proc_macros::hk_class;

#[hk_class]
#[parent(hkcdStaticTreeDynamicStoragehkcdStaticTreeCodec3Axis4)]
#[descendants(
    hkcdStaticMeshTreeBaseSection,
    hkcdStaticTreeTreehkcdStaticTreeDynamicStorage4
)]
pub struct hkcdStaticTreeDynamicStorage4 {}
