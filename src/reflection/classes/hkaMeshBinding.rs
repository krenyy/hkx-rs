use super::{hkaMeshBindingMapping::hkaMeshBindingMapping, hkaSkeleton::E_hkaSkeleton};
use crate::binary::types::{Array, GlobalPointer, StringPointer, Transform, VoidPointer};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hkaMeshBinding {
    pub mesh: VoidPointer, // TODO: GlobalPointer<E_hkxMesh>,
    pub originalSkeletonName: StringPointer,
    pub name: StringPointer,
    pub skeleton: GlobalPointer<E_hkaSkeleton>,
    pub mappings: Array<hkaMeshBindingMapping>,
    pub boneFromSkinMeshTransforms: Array<Transform>,
}
