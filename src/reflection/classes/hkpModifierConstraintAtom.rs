use super::hkpConstraintAtom::E_hkpConstraintAtom;
use crate::binary::types::GlobalPointer;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConstraintAtom)]
pub struct hkpModifierConstraintAtom {
    #[pad(16)]
    pub modifierAtomSize: u16,
    pub childSize: u16,
    pub child: GlobalPointer<E_hkpConstraintAtom>,
    #[assert(0)]
    _pad: u32,
}
