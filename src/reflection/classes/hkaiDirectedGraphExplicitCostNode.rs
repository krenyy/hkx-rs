use proc_macros::hk_class;

#[hk_class]
pub struct hkaiDirectedGraphExplicitCostNode {
    pub startEdgeIndex: i32,
    pub numEdges: i32,
}
