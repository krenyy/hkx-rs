use super::hclSimpleMeshBoneDeformOperatorTriangleBonePair::hclSimpleMeshBoneDeformOperatorTriangleBonePair;
use crate::binary::types::{Array, Matrix4};
use proc_macros::hk_class;

#[hk_class]
#[parent(hclOperator)]
pub struct hclSimpleMeshBoneDeformOperator {
    pub inputBufferIdx: u32,
    pub outputTransformSetIdx: u32,
    pub triangleBonePairs: Array<hclSimpleMeshBoneDeformOperatorTriangleBonePair>,
    pub localBoneTransforms: Array<Matrix4>,
}
