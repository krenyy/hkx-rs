use proc_macros::hk_class;

#[hk_class]
pub struct hclSimClothDataTransferMotionData {
    pub transformSetIndex: u32,
    pub transformIndex: u32,
    pub transferTranslationMotion: bool,
    #[pad(4)]
    pub minTranslationSpeed: f32,
    pub maxTranslationSpeed: f32,
    pub minTranslationBlend: f32,
    pub maxTranslationBlend: f32,
    pub transferRotationMotion: bool,
    #[pad(4)]
    pub minRotationSpeed: f32,
    pub maxRotationSpeed: f32,
    pub minRotationBlend: f32,
    pub maxRotationBlend: f32,
}
