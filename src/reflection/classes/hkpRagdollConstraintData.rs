use super::hkpRagdollConstraintDataAtoms::hkpRagdollConstraintDataAtoms;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConstraintData)]
pub struct hkpRagdollConstraintData {
    #[pad(16)]
    pub atoms: hkpRagdollConstraintDataAtoms,
}
