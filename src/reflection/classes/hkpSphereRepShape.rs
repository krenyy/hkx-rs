use proc_macros::hk_class;

#[hk_class]
#[parent(hkpShape)]
#[descendants(
    hkpBoxShape,
    hkpCapsuleShape,
    hkpConvexShape,
    hkpConvexTransformShape,
    hkpConvexTransformShapeBase,
    hkpConvexTranslateShape,
    hkpConvexVerticesShape,
    hkpCylinderShape,
    hkpSphereShape
)]
pub struct hkpSphereRepShape {}
