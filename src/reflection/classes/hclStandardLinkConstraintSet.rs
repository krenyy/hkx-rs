use super::hclStandardLinkConstraintSetLink::hclStandardLinkConstraintSetLink;
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclConstraintSet)]
pub struct hclStandardLinkConstraintSet {
    pub links: Array<hclStandardLinkConstraintSetLink>,
}
