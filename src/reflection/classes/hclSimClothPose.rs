use crate::binary::types::{Array, StringPointer, Vector4};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hclSimClothPose {
    pub name: StringPointer,
    pub positions: Array<Vector4>,
}
