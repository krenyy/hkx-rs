use crate::binary::types::Vector4;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConvexShape)]
pub struct hkpCapsuleShape {
    pub vertexA: Vector4,
    pub vertexB: Vector4,
}
