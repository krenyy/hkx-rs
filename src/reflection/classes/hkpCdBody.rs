use super::hkpShape::E_hkpShape;
use crate::binary::types::{GlobalPointer, VoidPointer};
use proc_macros::hk_class;

#[hk_class]
#[descendants(hkpCollidable, hkpLinkedCollidable)]
pub struct hkpCdBody {
    pub shape: GlobalPointer<E_hkpShape>,
    pub shapeKey: u32,
    _motion: VoidPointer,
    _parent: GlobalPointer<E_hkpCdBody>,
}
