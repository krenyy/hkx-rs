use super::{
    hkcdStaticMeshTreeBaseSectionDataRuns::hkcdStaticMeshTreeBaseSectionDataRuns,
    hkcdStaticMeshTreeBaseSectionPrimitives::hkcdStaticMeshTreeBaseSectionPrimitives,
    hkcdStaticMeshTreeBaseSectionSharedVertices::hkcdStaticMeshTreeBaseSectionSharedVertices,
};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkcdStaticTreeTreehkcdStaticTreeDynamicStorage4)]
pub struct hkcdStaticMeshTreeBaseSection {
    pub codecParms: [f32; 6],
    pub firstPackedVertex: u32,
    pub sharedVertices: hkcdStaticMeshTreeBaseSectionSharedVertices,
    pub primitives: hkcdStaticMeshTreeBaseSectionPrimitives,
    pub dataRuns: hkcdStaticMeshTreeBaseSectionDataRuns,
    pub numPackedVertices: u8,
    pub numSharedIndices: u8,
    pub leafIndex: u16,
    pub page: u8,
    pub flags: u8,
    pub layerData: u8,
    pub unusedData: u8,
}
