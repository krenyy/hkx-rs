use super::hclBendStiffnessConstraintSetLink::hclBendStiffnessConstraintSetLink;
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclConstraintSet)]
#[pad(4)]
pub struct hclBendStiffnessConstraintSet {
    pub links: Array<hclBendStiffnessConstraintSetLink>,
    pub useRestPoseConfig: bool,
}
