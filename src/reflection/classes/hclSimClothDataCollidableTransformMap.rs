use crate::binary::types::{Array, Matrix4};
use proc_macros::hk_class;

#[hk_class]
pub struct hclSimClothDataCollidableTransformMap {
    pub transformSetIndex: i32,
    pub transformIndices: Array<u32>,
    pub offsets: Array<Matrix4>,
}
