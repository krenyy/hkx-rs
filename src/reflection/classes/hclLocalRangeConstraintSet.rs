use super::hclLocalRangeConstraintSetLocalConstraint::hclLocalRangeConstraintSetLocalConstraint;
use crate::{binary::types::Array, reflection::enums::ShapeType::ShapeType};
use proc_macros::hk_class;

#[hk_class]
#[parent(hclConstraintSet)]
#[pad(4)]
pub struct hclLocalRangeConstraintSet {
    pub localConstraints: Array<hclLocalRangeConstraintSetLocalConstraint>,
    pub referenceMeshBufferIdx: u32,
    pub stiffness: f32,
    pub shapeType: ShapeType,
    pub applyNormalComponent: bool,
}
