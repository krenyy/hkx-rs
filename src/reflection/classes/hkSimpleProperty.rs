use super::hkSimplePropertyValue::hkSimplePropertyValue;
use proc_macros::hk_class;

#[hk_class]
pub struct hkSimpleProperty {
    pub key: u32,
    #[assert(0)]
    _alignentPadding: u32,
    pub value: hkSimplePropertyValue,
}
