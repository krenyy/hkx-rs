use crate::binary::types::StringPointer;
use proc_macros::hk_class;

#[hk_class]
#[pad(4)]
pub struct hkaBone {
    pub name: StringPointer,
    pub lockTranslation: bool,
}
