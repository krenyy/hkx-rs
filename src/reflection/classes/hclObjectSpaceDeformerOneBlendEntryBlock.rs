use proc_macros::hk_class;

#[hk_class]
pub struct hclObjectSpaceDeformerOneBlendEntryBlock {
    pub vertexIndices: [u16; 16],
    pub boneIndices: [u16; 16],
}
