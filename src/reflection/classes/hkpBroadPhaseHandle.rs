use proc_macros::hk_class;

#[hk_class]
#[descendants(hkpTypedBroadPhaseHandle)]
pub struct hkpBroadPhaseHandle {
    _id: u32,
}
