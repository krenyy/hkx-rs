use proc_macros::hk_class;

#[hk_class]
pub struct hclBoneSpaceDeformerTwoBlendEntryBlock {
    pub vertexIndices: [u16; 8],
    pub boneIndices: [u16; 16],
}
