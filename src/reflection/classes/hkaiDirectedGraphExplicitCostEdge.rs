use crate::binary::types::f16;
use proc_macros::hk_class;

#[hk_class]
pub struct hkaiDirectedGraphExplicitCostEdge {
    pub cost: f16,
    pub flags: u16,
    pub target: u32,
}
