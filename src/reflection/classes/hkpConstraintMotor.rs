use crate::reflection::enums::MotorType::MotorType;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
#[descendants(hkpLimitedForceConstraintMotor, hkpPositionConstraintMotor)]
#[pad(4)]
pub struct hkpConstraintMotor {
    pub r#type: MotorType,
}
