use super::{ActorInfo::ActorInfo, ShapeInfo::ShapeInfo};
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[signature = 0x5115a202]
pub struct StaticCompoundInfo {
    pub Offset: u32,
    pub ActorInfo: Array<ActorInfo>,
    pub ShapeInfo: Array<ShapeInfo>,
}
