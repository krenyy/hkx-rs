use super::hkUFloat8::hkUFloat8;
use crate::binary::types::{f16, Transform, Vector4};
use proc_macros::hk_class;

#[hk_class]
#[pad(4)]
pub struct hkMotionState {
    pub transform: Transform,
    pub sweptTransform: [Vector4; 5],
    pub deltaAngle: Vector4,
    pub objectRadius: f32,
    pub linearDamping: f16,
    pub angularDamping: f16,
    pub timeFactor: f16,
    pub maxLinearVelocity: hkUFloat8,
    pub maxAngularVelocity: hkUFloat8,
    pub deactivationClass: u8,
}
