use crate::binary::types::QsTransform;
use proc_macros::hk_class;

#[hk_class]
pub struct hkaSkeletonMapperDataSimpleMapping {
    pub boneA: i16,
    pub boneB: i16,
    pub aFromBTransform: QsTransform,
}
