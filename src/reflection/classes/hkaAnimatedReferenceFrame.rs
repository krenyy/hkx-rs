use crate::reflection::enums::hkaReferenceFrameTypeEnum::hkaReferenceFrameTypeEnum;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
#[pad(4)]
pub struct hkaAnimatedReferenceFrame {
    #[assert(crate::reflection::enums::hkaReferenceFrameTypeEnum::hkaReferenceFrameTypeEnum::REFERENCE_FRAME_UNKNOWN)]
    _frameType: hkaReferenceFrameTypeEnum,
}
