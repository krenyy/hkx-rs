use crate::binary::types::StringPointer;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
#[descendants(
    hclBendLinkConstraintSet,
    hclBendStiffnessConstraintSet,
    hclBonePlanesConstraintSet,
    hclCompressibleLinkConstraintSet,
    hclLocalRangeConstraintSet,
    hclStandardLinkConstraintSet,
    hclStretchLinkConstraintSet,
    hclTransitionConstraintSet,
    hclVolumeConstraint
)]
pub struct hclConstraintSet {
    pub name: StringPointer,
    #[assert(0)]
    _type: u32,
}
