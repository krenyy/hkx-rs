use proc_macros::hk_class;

#[hk_class]
pub struct hclObjectSpaceDeformerLocalBlockP {
    // Rust doesn't implement Default for ([T; N] | N > 32)
    // pub localPosition: [i16; 64],
    pub localPosition: [[i16; 32]; 2],
}
