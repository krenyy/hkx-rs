use proc_macros::hk_class;

#[hk_class]
pub struct hclBendStiffnessConstraintSetLink {
    pub weightA: f32,
    pub weightB: f32,
    pub weightC: f32,
    pub weightD: f32,
    pub bendStiffness: f32,
    pub restCurvature: f32,
    pub particleA: u16,
    pub particleB: u16,
    pub particleC: u16,
    pub particleD: u16,
}
