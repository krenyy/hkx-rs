use super::hkReferencedObject::E_hkReferencedObject;
use crate::binary::types::{GlobalPointer, StringPointer};
use proc_macros::hk_class;

#[hk_class]
pub struct hkRootLevelContainerNamedVariant {
    pub name: StringPointer,
    pub className: StringPointer,
    pub variant: GlobalPointer<E_hkReferencedObject>,
}
