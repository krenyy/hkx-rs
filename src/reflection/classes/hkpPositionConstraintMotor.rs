use proc_macros::hk_class;

#[hk_class]
#[parent(hkpLimitedForceConstraintMotor)]
pub struct hkpPositionConstraintMotor {
    pub tau: f32,
    pub damping: f32,
    pub proportionalRecoveryVelocity: f32,
    pub constantRecoveryVelocity: f32,
}
