use proc_macros::hk_class;

#[hk_class]
#[descendants(hkpSingleShapeContainer)]
pub struct hkpShapeContainer {
    _discard0: usize,
}
