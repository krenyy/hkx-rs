use super::hclBoneSpaceDeformer::hclBoneSpaceDeformer;
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclOperator)]
#[descendants(hclBoneSpaceSkinPOperator)]
pub struct hclBoneSpaceSkinOperator {
    pub transformSubset: Array<u16>,
    pub outputBufferIndex: u32,
    pub transformSetIndex: u32,
    pub boneSpaceDeformer: hclBoneSpaceDeformer,
}
