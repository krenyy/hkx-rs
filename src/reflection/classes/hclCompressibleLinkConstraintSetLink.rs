use proc_macros::hk_class;

#[hk_class]
pub struct hclCompressibleLinkConstraintSetLink {
    pub particleA: u16,
    pub particleB: u16,
    pub restLength: f32,
    pub compressionLength: f32,
    pub stiffness: f32,
}
