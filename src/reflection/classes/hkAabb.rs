use crate::binary::types::Vector4;
use proc_macros::hk_class;

#[hk_class]
pub struct hkAabb {
    pub min: Vector4,
    pub max: Vector4,
}
