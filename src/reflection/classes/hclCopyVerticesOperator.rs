use proc_macros::hk_class;

#[hk_class]
#[parent(hclOperator)]
#[pad(4)]
pub struct hclCopyVerticesOperator {
    pub inputBufferIdx: u32,
    pub outputBufferIdx: u32,
    pub numberOfVertices: u32,
    pub startVertexIn: u32,
    pub startVertexOut: u32,
    pub copyNormals: bool,
}
