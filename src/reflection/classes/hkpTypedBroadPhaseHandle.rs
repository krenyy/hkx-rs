use proc_macros::hk_class;

#[hk_class]
#[parent(hkpBroadPhaseHandle)]
pub struct hkpTypedBroadPhaseHandle {
    pub r#type: i8,
    #[assert(0)]
    _ownerOffset: i8,
    pub objectQualityType: i8,
    #[pad(4)]
    pub collisionFilterInfo: u32,
}
