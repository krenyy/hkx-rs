use super::hclCompressibleLinkConstraintSetLink::hclCompressibleLinkConstraintSetLink;
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclConstraintSet)]
pub struct hclCompressibleLinkConstraintSet {
    pub links: Array<hclCompressibleLinkConstraintSetLink>,
}
