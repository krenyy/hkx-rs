use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConstraintAtom)]
pub struct hkpAngLimitConstraintAtom {
    pub isEnabled: u8,
    pub limitAxis: u8,
    pub cosineAxis: u8,
    #[assert(0, 0, 0)]
    _padding2: [u8; 3],
    pub minAngle: f32,
    pub maxAngle: f32,
    pub angularLimitsTauFactor: f32,
    pub angularLimitsDampFactor: f32,
    #[assert(0, 0, 0, 0, 0, 0, 0, 0)]
    _padding: [u8; 8],
}
