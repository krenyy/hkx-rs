use crate::reflection::enums::AtomType::AtomType;
use proc_macros::hk_class;

#[hk_class]
#[descendants(
    hkp2dAngConstraintAtom,
    hkpAngFrictionConstraintAtom,
    hkpAngLimitConstraintAtom,
    hkpAngMotorConstraintAtom,
    hkpBallSocketConstraintAtom,
    hkpConeLimitConstraintAtom,
    hkpModifierConstraintAtom,
    hkpRagdollMotorConstraintAtom,
    hkpSetLocalTransformsConstraintAtom,
    hkpSetupStabilizationAtom,
    hkpTwistLimitConstraintAtom
)]
#[pad(2)]
pub struct hkpConstraintAtom {
    pub r#type: AtomType,
}
