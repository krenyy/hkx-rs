use crate::reflection::enums::SlotFlags::SlotFlags;
use proc_macros::hk_class;

#[hk_class]
pub struct hclBufferLayoutSlot {
    pub flags: SlotFlags,
    pub stride: u8,
}
