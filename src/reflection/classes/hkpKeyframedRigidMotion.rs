use proc_macros::hk_class;

#[hk_class]
#[parent(hkpMotion)]
#[descendants(hkpMaxSizeMotion)]
pub struct hkpKeyframedRigidMotion {}
