use super::hclStretchLinkConstraintSetLink::hclStretchLinkConstraintSetLink;
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclConstraintSet)]
pub struct hclStretchLinkConstraintSet {
    pub links: Array<hclStretchLinkConstraintSetLink>,
}
