use super::hclMoveParticlesOperatorVertexParticlePair::hclMoveParticlesOperatorVertexParticlePair;
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclOperator)]
pub struct hclMoveParticlesOperator {
    pub vertexParticlePairs: Array<hclMoveParticlesOperatorVertexParticlePair>,
    pub simClothIndex: u32,
    pub refBufferIdx: u32,
}
