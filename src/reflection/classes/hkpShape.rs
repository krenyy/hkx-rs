use proc_macros::hk_class;

#[hk_class]
#[parent(hkpShapeBase)]
#[descendants(
    hkpBoxShape,
    hkpBvCompressedMeshShape,
    hkpBvTreeShape,
    hkpCapsuleShape,
    hkpConvexShape,
    hkpConvexTransformShape,
    hkpConvexTransformShapeBase,
    hkpConvexTranslateShape,
    hkpConvexVerticesShape,
    hkpCylinderShape,
    hkpListShape,
    hkpShapeCollection,
    hkpSphereRepShape,
    hkpSphereShape,
    hkpStaticCompoundShape
)]
pub struct hkpShape {
    pub userData: usize,
}
