use proc_macros::hk_class;

#[hk_class]
pub struct hkaiStreamingSetNavMeshConnection {
    pub faceIndex: i32,
    pub edgeIndex: i32,
    pub oppositeFaceIndex: i32,
    pub oppositeEdgeIndex: i32,
}
