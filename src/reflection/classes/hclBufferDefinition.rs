use super::hclBufferLayout::hclBufferLayout;
use crate::binary::types::StringPointer;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
#[descendants(hclScratchBufferDefinition)]
pub struct hclBufferDefinition {
    pub name: StringPointer,
    pub r#type: i32,
    pub subType: i32,
    pub numVertices: u32,
    pub numTriangles: u32,
    pub bufferLayout: hclBufferLayout,
}
