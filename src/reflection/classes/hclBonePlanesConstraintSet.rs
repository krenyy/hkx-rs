use super::hclBonePlanesConstraintSetBonePlane::hclBonePlanesConstraintSetBonePlane;
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hclConstraintSet)]
pub struct hclBonePlanesConstraintSet {
    pub bonePlanes: Array<hclBonePlanesConstraintSetBonePlane>,
    pub transformSetIndex: u32,
}
