use proc_macros::hk_class;

#[hk_class]
#[descendants(
    hkcdStaticTreeCodec3Axis4,
    hkcdStaticTreeCodec3Axis5,
    hkcdStaticTreeCodec3Axis6
)]
pub struct hkcdStaticTreeCodec3Axis {
    pub xyz: [u8; 3],
}
