use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConstraintAtom)]
pub struct hkpAngFrictionConstraintAtom {
    pub isEnabled: u8,
    pub firstFrictionAxis: u8,
    pub numFrictionAxes: u8,
    #[pad(4)]
    pub maxFrictionTorque: f32,
    #[assert(0, 0, 0, 0)]
    _padding: [u8; 4],
}
