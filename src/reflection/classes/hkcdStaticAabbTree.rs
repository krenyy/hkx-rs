use super::hkcdStaticTreeDefaultTreeStorage6::E_hkcdStaticTreeDefaultTreeStorage6;
use crate::binary::types::GlobalPointer;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hkcdStaticAabbTree {
    #[assert(false)]
    _shouldDeleteTree: bool,
    pub treePtr: GlobalPointer<E_hkcdStaticTreeDefaultTreeStorage6>,
}
