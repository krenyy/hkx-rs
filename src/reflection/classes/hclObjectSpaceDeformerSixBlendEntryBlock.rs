use proc_macros::hk_class;

#[hk_class]
pub struct hclObjectSpaceDeformerSixBlendEntryBlock {
    pub vertexIndices: [u16; 16],
    // Rust doesn't implement Default for ([T; N] | N > 32)
    // pub boneIndices: [u16; 128],
    // pub boneWeights: [u16; 128],
    pub boneIndices: [[u16; 16]; 6],
    pub boneWeights: [[u16; 16]; 6],
}
