use super::hkRootLevelContainerNamedVariant::hkRootLevelContainerNamedVariant;
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[signature = 0x2772c11e]
pub struct hkRootLevelContainer {
    pub namedVariants: Array<hkRootLevelContainerNamedVariant>,
}
