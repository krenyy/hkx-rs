use proc_macros::hk_class;

#[hk_class]
pub struct hkcdStaticMeshTreeBaseSectionDataRuns {
    pub data: u32,
}
