use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConstraintMotor)]
#[descendants(hkpPositionConstraintMotor)]
pub struct hkpLimitedForceConstraintMotor {
    pub minForce: f32,
    pub maxForce: f32,
}
