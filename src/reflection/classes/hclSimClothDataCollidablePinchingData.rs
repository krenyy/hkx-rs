use proc_macros::hk_class;

#[hk_class]
pub struct hclSimClothDataCollidablePinchingData {
    pub pinchDetectionEnabled: bool,
    pub pinchDetectionPriority: i8,
    #[pad(4)]
    pub pinchDetectionRadius: f32,
}
