use proc_macros::hk_class;

#[hk_class]
pub struct hclBufferUsage {
    pub perComponentFlags: [u8; 4],
    pub trianglesRead: bool,
}
