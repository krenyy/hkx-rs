use proc_macros::hk_class;

#[hk_class]
pub struct hkUFloat8 {
    pub value: u8,
}
