use crate::{binary::types::VoidPointer, reflection::enums::CollectionType::CollectionType};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpShape)]
#[descendants(hkpListShape)]
#[pad(4)]
pub struct hkpShapeCollection {
    _discard0: VoidPointer, // FIXME: no idea
    pub disableWelding: bool,
    pub collectionType: CollectionType,
}
