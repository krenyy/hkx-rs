use super::hkLocalFrame::E_hkLocalFrame;
use crate::binary::types::GlobalPointer;
use proc_macros::hk_class;

#[hk_class]
#[pad(4)]
pub struct hkaSkeletonLocalFrameOnBone {
    pub localFrame: GlobalPointer<E_hkLocalFrame>,
    pub boneIndex: i16,
}
