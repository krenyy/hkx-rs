use crate::{binary::types::f16, reflection::enums::ResponseType::ResponseType};
use proc_macros::hk_class;

#[hk_class]
pub struct hkpMaterial {
    pub responseType: ResponseType,
    #[pad(2)]
    pub rollingFrictionMultiplier: f16,
    pub friction: f32,
    pub restitution: f32,
}
