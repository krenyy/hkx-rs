use crate::reflection::enums::{
    ShapeDispatchTypeEnum::ShapeDispatchTypeEnum, ShapeInfoCodecTypeEnum::ShapeInfoCodecTypeEnum,
};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
#[descendants(
    hkpBoxShape,
    hkpBvCompressedMeshShape,
    hkpBvTreeShape,
    hkpCapsuleShape,
    hkpConvexShape,
    hkpConvexTransformShape,
    hkpConvexTransformShapeBase,
    hkpConvexTranslateShape,
    hkpConvexVerticesShape,
    hkpCylinderShape,
    hkpListShape,
    hkpShape,
    hkpShapeBase,
    hkpShapeCollection,
    hkpSphereRepShape,
    hkpSphereShape,
    hkpStaticCompoundShape
)]
pub struct hkcdShape {
    _type: u8,
    pub dispatchType: ShapeDispatchTypeEnum,
    pub bitsPerKey: u8,
    pub shapeInfoCodecType: ShapeInfoCodecTypeEnum,
}
