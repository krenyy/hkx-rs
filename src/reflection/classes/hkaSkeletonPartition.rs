use crate::binary::types::StringPointer;
use proc_macros::hk_class;

#[hk_class]
pub struct hkaSkeletonPartition {
    pub name: StringPointer,
    pub startBoneIndex: i16,
    pub numBones: i16,
}
