use crate::binary::types::QsTransform;
use proc_macros::hk_class;

#[hk_class]
pub struct hkaSkeletonMapperDataChainMapping {
    pub startBoneA: i16,
    pub endBoneA: i16,
    pub startBoneB: i16,
    pub endBoneB: i16,
    pub startAFromBTransform: QsTransform,
    pub endAFromBTransform: QsTransform,
}
