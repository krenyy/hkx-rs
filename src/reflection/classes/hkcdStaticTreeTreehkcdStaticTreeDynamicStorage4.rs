use super::hkAabb::hkAabb;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkcdStaticTreeDynamicStorage4)]
#[descendants(hkcdStaticMeshTreeBaseSection)]
pub struct hkcdStaticTreeTreehkcdStaticTreeDynamicStorage4 {
    pub domain: hkAabb,
}
