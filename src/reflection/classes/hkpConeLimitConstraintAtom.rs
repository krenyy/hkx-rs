use crate::reflection::enums::MeasurementMode::MeasurementMode;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConstraintAtom)]
pub struct hkpConeLimitConstraintAtom {
    pub isEnabled: u8,
    pub twistAxisInA: u8,
    pub refAxisInB: u8,
    pub angleMeasurementMode: MeasurementMode,
    pub memOffsetToAngleOffset: u8,
    #[pad(4)]
    pub minAngle: f32,
    pub maxAngle: f32,
    pub angularLimitsTauFactor: f32,
    pub angularLimitsDampFactor: f32,
    #[assert(0, 0, 0, 0, 0, 0, 0, 0)]
    _padding: [u8; 8],
}
