use super::{hkaiNavMesh::E_hkaiNavMesh, hkcdStaticAabbTree::E_hkcdStaticAabbTree};
use crate::binary::types::GlobalPointer;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkaiNavMeshQueryMediator)]
pub struct hkaiStaticTreeNavMeshQueryMediator {
    pub tree: GlobalPointer<E_hkcdStaticAabbTree>,
    pub navMesh: GlobalPointer<E_hkaiNavMesh>,
}
