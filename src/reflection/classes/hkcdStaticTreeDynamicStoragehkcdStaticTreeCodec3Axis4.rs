use super::hkcdStaticTreeCodec3Axis4::hkcdStaticTreeCodec3Axis4;
use proc_macros::hk_class;

#[hk_class]
#[descendants(
    hkcdStaticMeshTreeBaseSection,
    hkcdStaticTreeDynamicStorage4,
    hkcdStaticTreeTreehkcdStaticTreeDynamicStorage4
)]
#[pad(16)]
pub struct hkcdStaticTreeDynamicStoragehkcdStaticTreeCodec3Axis4 {
    pub nodes: hkcdStaticTreeCodec3Axis4,
}
