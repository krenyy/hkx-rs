use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
pub struct hkBitFieldStoragehkArrayunsignedinthkContainerHeapAllocator {
    pub words: Array<u32>,
    pub numBits: i32,
}
