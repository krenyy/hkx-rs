use super::{
    hclBoneSpaceDeformerFourBlendEntryBlock::hclBoneSpaceDeformerFourBlendEntryBlock,
    hclBoneSpaceDeformerOneBlendEntryBlock::hclBoneSpaceDeformerOneBlendEntryBlock,
    hclBoneSpaceDeformerThreeBlendEntryBlock::hclBoneSpaceDeformerThreeBlendEntryBlock,
    hclBoneSpaceDeformerTwoBlendEntryBlock::hclBoneSpaceDeformerTwoBlendEntryBlock,
};
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[pad(2)]
pub struct hclBoneSpaceDeformer {
    pub fourBlendEntries: Array<hclBoneSpaceDeformerFourBlendEntryBlock>,
    pub threeBlendEntries: Array<hclBoneSpaceDeformerThreeBlendEntryBlock>,
    pub twoBlendEntries: Array<hclBoneSpaceDeformerTwoBlendEntryBlock>,
    pub oneBlendEntries: Array<hclBoneSpaceDeformerOneBlendEntryBlock>,
    pub controlBytes: Array<u8>,
    pub startVertexIndex: u16,
    pub endVertexIndex: u16,
    pub batchSizeSpu: u16,
    pub partialWrite: bool,
}
