use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConstraintAtom)]
#[pad(4)]
pub struct hkp2dAngConstraintAtom {
    pub freeRotationAxis: u8,
    #[assert(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
    _padding: [u8; 12], // havok employees casually fucking up the padding (sum is 15 bytes)
}
