use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
pub struct hkaMeshBindingMapping {
    pub mappings: Array<i16>,
}
