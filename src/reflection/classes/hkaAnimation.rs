use super::{
    hkaAnimatedReferenceFrame::E_hkaAnimatedReferenceFrame, hkaAnnotationTrack::hkaAnnotationTrack,
};
use crate::{
    binary::types::{Array, GlobalPointer},
    reflection::enums::AnimationType::AnimationType,
};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hkaAnimation {
    pub r#type: AnimationType,
    pub duration: f32,
    pub numberOfTransformTracks: i32,
    pub numberOfFloatTracks: i32,
    pub extractedMotion: GlobalPointer<E_hkaAnimatedReferenceFrame>,
    pub annotationTracks: Array<hkaAnnotationTrack>,
}
