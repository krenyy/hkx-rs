use proc_macros::hk_class;

#[hk_class]
pub struct hclLocalRangeConstraintSetLocalConstraint {
    pub particleIndex: u16,
    pub referenceVertex: u16,
    pub maximumDistance: f32,
    pub maxNormalDistance: f32,
    pub minNormalDistance: f32,
}
