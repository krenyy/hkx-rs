use crate::binary::types::{QsTransform, Vector4};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConvexTransformShapeBase)]
pub struct hkpConvexTransformShape {
    pub transform: QsTransform,
    pub extraScale: Vector4,
}
