use super::hkReferencedObject::E_hkReferencedObject;
use crate::binary::types::{GlobalPointer, StringPointer, Matrix4};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
#[pad(4)]
pub struct hkaBoneAttachment {
    pub originalSkeletonName: StringPointer,
    pub boneFromAttachment: Matrix4,
    pub attachment: GlobalPointer<E_hkReferencedObject>,
    pub name: StringPointer,
    pub boneIndex: i16,
}
