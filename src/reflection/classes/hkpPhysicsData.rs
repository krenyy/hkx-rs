use super::{hkReferencedObject::E_hkReferencedObject, hkpPhysicsSystem::E_hkpPhysicsSystem};
use crate::binary::types::{Array, GlobalPointer};
use proc_macros::hk_class;

#[hk_class]
#[signature = 0x47a8ca83]
#[parent(hkReferencedObject)]
pub struct hkpPhysicsData {
    pub worldCinfo: GlobalPointer<E_hkReferencedObject>, // TODO: E_hkpWorldCinfo
    pub systems: Array<GlobalPointer<E_hkpPhysicsSystem>>,
}
