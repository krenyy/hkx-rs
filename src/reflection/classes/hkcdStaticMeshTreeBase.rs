use super::{
    hkcdStaticMeshTreeBasePrimitive::hkcdStaticMeshTreeBasePrimitive,
    hkcdStaticMeshTreeBaseSection::hkcdStaticMeshTreeBaseSection,
};
use crate::binary::types::Array;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkcdStaticTreeTreehkcdStaticTreeDynamicStorage5)]
#[descendants(
    hkcdStaticMeshTreehkcdStaticMeshTreeCommonConfigunsignedintunsignedlonglong1121hkpBvCompressedMeshShapeTreeDataRun,
    hkpBvCompressedMeshShapeTree

    )]
pub struct hkcdStaticMeshTreeBase {
    pub numPrimitiveKeys: i32,
    pub bitsPerKey: i32,
    pub maxKeyValue: u32,
    pub sections: Array<hkcdStaticMeshTreeBaseSection>,
    pub primitives: Array<hkcdStaticMeshTreeBasePrimitive>,
    pub sharedVerticesIndex: Array<u16>,
}
