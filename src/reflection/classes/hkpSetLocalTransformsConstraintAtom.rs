use crate::binary::types::Transform;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConstraintAtom)]
pub struct hkpSetLocalTransformsConstraintAtom {
    pub transformA: Transform,
    pub transformB: Transform,
}
