use super::hkaAnimation::E_hkaAnimation;
use crate::{
    binary::types::{Array, GlobalPointer, StringPointer},
    reflection::enums::BlendHint::BlendHint,
};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hkaAnimationBinding {
    pub originalSkeletonName: StringPointer,
    pub animation: GlobalPointer<E_hkaAnimation>,
    pub transformTrackToBoneIndices: Array<i16>,
    pub floatTrackToFloatSlotIndices: Array<i16>,
    pub partitionIndices: Array<i16>,
    pub blendHint: BlendHint,
}
