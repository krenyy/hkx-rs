use super::hkpShape::E_hkpShape;
use crate::binary::types::GlobalPointer;
use proc_macros::hk_class;

#[hk_class]
#[pad(16)]
pub struct hkpListShapeChildInfo {
    pub shape: GlobalPointer<E_hkpShape>,
    pub collisionFilterInfo: u32,
    pub shapeInfo: u16,
    #[assert(0)]
    _shapeSize: u16,
    #[assert(0)]
    _numChildShapes: i32,
}
