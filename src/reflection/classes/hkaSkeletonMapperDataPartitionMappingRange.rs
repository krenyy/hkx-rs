use proc_macros::hk_class;

#[hk_class]
pub struct hkaSkeletonMapperDataPartitionMappingRange {
    pub startMappingIndex: i32,
    pub numMappings: i32,
}
