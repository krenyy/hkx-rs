use crate::binary::types::VoidPointer;
use proc_macros::hk_class;

#[hk_class]
pub struct hkpShapeKeyTable {
    lists: VoidPointer, // TODO: GlobalPointer<hkpShapeKeyTableBlock>
    occupancyBitField: u32,
}
