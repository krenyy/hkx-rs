use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
#[descendants(hclCapsuleShape, hclPlaneShape, hclSphereShape)]
pub struct hclShape {
    #[assert(0)]
    _type: i32,
}
