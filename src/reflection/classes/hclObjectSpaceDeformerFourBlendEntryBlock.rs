use proc_macros::hk_class;

#[hk_class]
pub struct hclObjectSpaceDeformerFourBlendEntryBlock {
    pub vertexIndices: [u16; 16],
    // Rust doesn't implement Default for ([T; N] | N > 32)
    // pub boneIndices: [u16; 128],
    // pub boneWeights: [u16; 128],
    pub boneIndices: [[u16; 16]; 4],
    pub boneWeights: [[u8; 16]; 4],
}
