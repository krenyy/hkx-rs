use super::{hclClothData::E_hclClothData, hclCollidable::E_hclCollidable};
use crate::binary::types::{Array, GlobalPointer};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hclClothContainer {
    pub collidables: Array<GlobalPointer<E_hclCollidable>>,
    pub clothDatas: Array<GlobalPointer<E_hclClothData>>,
}
