use crate::reflection::enums::BvTreeType::BvTreeType;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpShape)]
#[descendants(hkpBvCompressedMeshShape, hkpStaticCompoundShape)]
#[pad(4)]
pub struct hkpBvTreeShape {
    pub bvTreeType: BvTreeType,
}
