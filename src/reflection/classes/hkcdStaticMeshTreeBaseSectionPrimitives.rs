use proc_macros::hk_class;

#[hk_class]
pub struct hkcdStaticMeshTreeBaseSectionPrimitives {
    pub data: u32,
}
