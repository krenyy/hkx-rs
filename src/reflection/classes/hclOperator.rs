use crate::binary::types::StringPointer;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
#[descendants(
    hclBoneSpaceSkinOperator,
    hclBoneSpaceSkinPOperator,
    hclCopyVerticesOperator,
    hclGatherAllVerticesOperator,
    hclMoveParticlesOperator,
    hclObjectSpaceSkinOperator,
    hclObjectSpaceSkinPOperator,
    hclSimpleMeshBoneDeformOperator,
    hclSimulateOperator
)]
pub struct hclOperator {
    pub name: StringPointer,
    #[assert(0)]
    _type: u32,
}
