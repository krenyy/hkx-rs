use super::{
    hkp2dAngConstraintAtom::hkp2dAngConstraintAtom,
    hkpAngFrictionConstraintAtom::hkpAngFrictionConstraintAtom,
    hkpAngLimitConstraintAtom::hkpAngLimitConstraintAtom,
    hkpAngMotorConstraintAtom::hkpAngMotorConstraintAtom,
    hkpBallSocketConstraintAtom::hkpBallSocketConstraintAtom,
    hkpSetLocalTransformsConstraintAtom::hkpSetLocalTransformsConstraintAtom,
    hkpSetupStabilizationAtom::hkpSetupStabilizationAtom,
};
use proc_macros::hk_class;

#[hk_class]
pub struct hkpLimitedHingeConstraintDataAtoms {
    pub transforms: hkpSetLocalTransformsConstraintAtom,
    pub setupStabilization: hkpSetupStabilizationAtom,
    pub angMotor: hkpAngMotorConstraintAtom,
    pub angFriction: hkpAngFrictionConstraintAtom,
    pub angLimit: hkpAngLimitConstraintAtom,
    pub _2dAng: hkp2dAngConstraintAtom,
    pub ballSocket: hkpBallSocketConstraintAtom,
}
