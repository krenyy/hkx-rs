use proc_macros::hk_class;

#[hk_class]
#[parent(hkcdShape)]
#[descendants(
    hkpBoxShape,
    hkpBvCompressedMeshShape,
    hkpBvTreeShape,
    hkpCapsuleShape,
    hkpConvexShape,
    hkpConvexTransformShape,
    hkpConvexTransformShapeBase,
    hkpConvexTranslateShape,
    hkpConvexVerticesShape,
    hkpCylinderShape,
    hkpListShape,
    hkpShape,
    hkpShapeCollection,
    hkpSphereRepShape,
    hkpSphereShape,
    hkpStaticCompoundShape
)]
pub struct hkpShapeBase {}
