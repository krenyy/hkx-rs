use super::{
    hkpAngFrictionConstraintAtom::hkpAngFrictionConstraintAtom,
    hkpBallSocketConstraintAtom::hkpBallSocketConstraintAtom,
    hkpConeLimitConstraintAtom::hkpConeLimitConstraintAtom,
    hkpRagdollMotorConstraintAtom::hkpRagdollMotorConstraintAtom,
    hkpSetLocalTransformsConstraintAtom::hkpSetLocalTransformsConstraintAtom,
    hkpSetupStabilizationAtom::hkpSetupStabilizationAtom,
    hkpTwistLimitConstraintAtom::hkpTwistLimitConstraintAtom,
};
use proc_macros::hk_class;

#[hk_class]
pub struct hkpRagdollConstraintDataAtoms {
    pub transforms: hkpSetLocalTransformsConstraintAtom,
    pub setupStabilization: hkpSetupStabilizationAtom,
    pub ragdollMotors: hkpRagdollMotorConstraintAtom,
    pub angFriction: hkpAngFrictionConstraintAtom,
    pub twistLimit: hkpTwistLimitConstraintAtom,
    pub coneLimit: hkpConeLimitConstraintAtom,
    pub planesLimit: hkpConeLimitConstraintAtom,
    pub ballSocket: hkpBallSocketConstraintAtom,
}
