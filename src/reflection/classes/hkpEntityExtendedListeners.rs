use super::hkpEntitySmallArraySerializeOverrideType::hkpEntitySmallArraySerializeOverrideType;
use proc_macros::hk_class;

#[hk_class]
pub struct hkpEntityExtendedListeners {
    _activationListeners: hkpEntitySmallArraySerializeOverrideType,
    _entityListeners: hkpEntitySmallArraySerializeOverrideType,
}
