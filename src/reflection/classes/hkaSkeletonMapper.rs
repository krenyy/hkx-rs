use super::hkaSkeletonMapperData::hkaSkeletonMapperData;
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hkaSkeletonMapper {
    #[pad(16)] // FIXME: no idea why :(
    pub mapping: hkaSkeletonMapperData,
}
