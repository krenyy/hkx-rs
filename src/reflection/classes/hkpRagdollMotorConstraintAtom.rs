use super::hkpConstraintMotor::E_hkpConstraintMotor;
use crate::binary::types::{GlobalPointer, Matrix3, VoidPointer};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkpConstraintAtom)]
pub struct hkpRagdollMotorConstraintAtom {
    pub isEnabled: bool,
    #[pad(2)]
    #[assert(0)]
    _initializedOffset: i16,
    #[assert(0)]
    _previousTargetAnglesOffset: i16,
    pub target_bRca: Matrix3,
    pub motors: [GlobalPointer<E_hkpConstraintMotor>; 3],
    _discard0: VoidPointer, // FIXME: no idea
}
