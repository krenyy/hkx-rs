use crate::binary::types::VoidPointer;
use proc_macros::hk_class;

#[hk_class]
pub struct hkpCollidableBoundingVolumeData {
    pub min: [u32; 3],
    pub expansionMin: [u8; 3],
    pub expansionShift: u8,
    pub max: [u32; 3],
    pub expansionMax: [u8; 3],
    #[assert(0)]
    _padding: u8,
    #[assert(0)]
    _numChildShapeAabbs: u16,
    #[assert(0)]
    _capacityChildShapeAabbs: u16,
    _childShapeAabbs: VoidPointer,
    _childShapeKeys: VoidPointer,
}
