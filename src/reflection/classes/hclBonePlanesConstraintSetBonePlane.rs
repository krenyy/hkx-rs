use crate::binary::types::Vector4;
use proc_macros::hk_class;

#[hk_class]
pub struct hclBonePlanesConstraintSetBonePlane {
    pub planeEquationBone: Vector4,
    pub particleIndex: u16,
    pub transformIndex: u16,
    pub stiffness: f32,
}
