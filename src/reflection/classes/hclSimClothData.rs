use super::{
    hclAction::E_hclAction, hclCollidable::E_hclCollidable, hclConstraintSet::E_hclConstraintSet,
    hclSimClothDataCollidablePinchingData::hclSimClothDataCollidablePinchingData,
    hclSimClothDataCollidableTransformMap::hclSimClothDataCollidableTransformMap,
    hclSimClothDataLandscapeCollisionData::hclSimClothDataLandscapeCollisionData,
    hclSimClothDataOverridableSimulationInfo::hclSimClothDataOverridableSimulationInfo,
    hclSimClothDataParticleData::hclSimClothDataParticleData,
    hclSimClothDataTransferMotionData::hclSimClothDataTransferMotionData,
    hclSimClothPose::E_hclSimClothPose,
};
use crate::binary::types::{Array, GlobalPointer, StringPointer};
use proc_macros::hk_class;

#[hk_class]
#[parent(hkReferencedObject)]
pub struct hclSimClothData {
    pub simulationInfo: hclSimClothDataOverridableSimulationInfo,
    pub name: StringPointer,
    pub particleDatas: Array<hclSimClothDataParticleData>,
    pub fixedParticles: Array<u16>,
    pub triangleIndices: Array<u16>,
    pub triangleFlips: Array<u8>,
    pub totalMass: f32,
    #[pad]
    pub collidableTransformMap: hclSimClothDataCollidableTransformMap,
    pub perInstanceCollidables: Array<GlobalPointer<E_hclCollidable>>,
    pub staticConstraintSets: Array<GlobalPointer<E_hclConstraintSet>>,
    pub antiPinchConstraintSets: Array<GlobalPointer<E_hclConstraintSet>>,
    pub simClothPoses: Array<GlobalPointer<E_hclSimClothPose>>,
    pub actions: Array<GlobalPointer<E_hclAction>>,
    pub staticCollisionMasks: Array<u32>,
    pub perParticlePinchDetectionEnabledFlags: Array<bool>,
    pub collidablePinchingDatas: Array<hclSimClothDataCollidablePinchingData>,
    pub minPinchedParticleIndex: u16,
    pub maxPinchedParticleIndex: u16,
    pub maxCollisionPairs: u32,
    pub maxParticleRadius: f32,
    pub landscapeCollisionData: hclSimClothDataLandscapeCollisionData,
    pub numLandscapeCollidableParticles: u32,
    pub doNormals: bool,
    #[pad(4)]
    pub transferMotionData: hclSimClothDataTransferMotionData,
}
