use proc_macros::hk_class;

#[hk_class]
#[parent(hkcdStaticTreeDynamicStoragehkcdStaticTreeCodec3Axis5)]
#[descendants(
    hkcdStaticMeshTreeBase,
    hkcdStaticMeshTreehkcdStaticMeshTreeCommonConfigunsignedintunsignedlonglong1121hkpBvCompressedMeshShapeTreeDataRun,
    hkcdStaticTreeTreehkcdStaticTreeDynamicStorage5,
    hkpBvCompressedMeshShapeTree
)]
pub struct hkcdStaticTreeDynamicStorage5 {}
