use proc_macros::hk_class;

#[hk_class]
pub struct hkcdStaticMeshTreeBaseSectionSharedVertices {
    pub data: u32,
}
