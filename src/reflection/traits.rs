pub trait ClassInfo {
    fn name(&self) -> String {
        "".to_string()
    }

    fn signature(&self) -> u32 {
        0x0
    }
}
