use crate::hk_enum;

hk_enum!(
    u8,
    BvTreeType,
    0 => BVTREE_MOPP,
    1 => BVTREE_TRISAMPLED_HEIGHTFIELD,
    2 => BVTREE_STATIC_COMPOUND,
    3 => BVTREE_COMPRESSED_MESH,
    4 => BVTREE_USER,
    5 => BVTREE_MAX
);
