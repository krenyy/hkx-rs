use crate::hk_enum;

hk_enum!(
    i8,
    BlendHint,
    0 => NORMAL,
    1 => ADDITIVE_DEPRECATED,
    2 => ADDITIVE
);
