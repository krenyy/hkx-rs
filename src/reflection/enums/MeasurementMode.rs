use crate::hk_enum;

hk_enum!(
    u8,
    MeasurementMode,
    0 => ZERO_WHEN_VECTORS_ALIGNED,
    1 => ZERO_WHEN_VECTORS_PERPENDICULAR
);
