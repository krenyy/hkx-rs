use crate::hk_enum;

hk_enum!(
    u8,
    EdgeFlagBits,
    1 => EDGE_SILHOUETTE,
    2 => EDGE_RETRIANGULATED,
    4 => EDGE_ORIGINAL,
    8 => OPPOSITE_EDGE_UNLOADED_UNUSED,
    16 => EDGE_USER,
    32 => EDGE_BLOCKED,
    64 => EDGE_EXTERNAL_OPPOSITE
);
