use crate::hk_enum;

hk_enum!(
    i32,
    AnimationType,
    0 => HK_UNKNOWN_ANIMATION,
    1 => HK_INTERLEAVED_ANIMATION,
    2 => HK_MIRRORED_ANIMATION,
    3 => HK_SPLINE_COMPRESSED_ANIMATION,
    4 => HK_QUANTIZED_COMPRESSED_ANIMATION,
    5 => HK_PREDICTIVE_COMPRESSED_ANIMATION,
    6 => HK_REFERENCE_POSE_ANIMATION
);
