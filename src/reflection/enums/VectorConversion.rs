use crate::hk_enum;

hk_enum!(
    u8,
    VectorConversion,
    0 => VC_FLOAT4,
    1 => VC_FLOAT3,
    2 => VC_BYTE4,
    3 => VC_SHORT3,
    4 => VC_HFLOAT3,
    20 => VC_CUSTOM_A,
    21 => VC_CUSTOM_B,
    22 => VC_CUSTOM_C,
    23 => VC_CUSTOM_D,
    24 => VC_CUSTOM_E,
    250 => VC_NONE
);
