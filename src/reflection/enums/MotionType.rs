use crate::hk_enum;

hk_enum!(
    u8,
    MotionType,
    0 => MOTION_INVALID,
    1 => MOTION_DYNAMIC,
    2 => MOTION_SPHERE_INERTIA,
    3 => MOTION_BOX_INERTIA,
    4 => MOTION_KEYFRAMED,
    5 => MOTION_FIXED,
    6 => MOTION_THIN_BOX_INERTIA,
    7 => MOTION_CHARACTER,
    8 => MOTION_MAX_ID
);
