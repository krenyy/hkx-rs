use crate::hk_enum;

hk_enum!(
    u8,
    SolvingMethod,
    0 => METHOD_STABILIZED,
    1 => METHOD_OLD
);
