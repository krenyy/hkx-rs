use crate::hk_enum;

hk_enum!(
    u32,
    ShapeType,
    0 => SHAPE_SPHERE,
    1 => SHAPE_CYLINDER
);
