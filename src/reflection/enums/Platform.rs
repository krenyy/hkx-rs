use crate::hk_enum;

hk_enum!(
    u32,
    Platform,
    0 => HCL_PLATFORM_INVALID,
    1 => HCL_PLATFORM_WIN32,
    2 => HCL_PLATFORM_X64,
    4 => HCL_PLATFORM_MACPPC,
    8 => HCL_PLATFORM_IOS,
    16 => HCL_PLATFORM_MAC386,
    32 => HCL_PLATFORM_PS3,
    64 => HCL_PLATFORM_XBOX360,
    128 => HCL_PLATFORM_WII,
    256 => HCL_PLATFORM_LRB,
    512 => HCL_PLATFORM_LINUX,
    1024 => HCL_PLATFORM_PSVITA,
    2048 => HCL_PLATFORM_ANDROID,
    4096 => HCL_PLATFORM_CTR,
    8192 => HCL_PLATFORM_WIIU,
    16384 => HCL_PLATFORM_PS4,
    32768 => HCL_PLATFORM_XBOXONE,
    65536 => HCL_PLATFORM_MAC64,
    131072 => HCL_PLATFORM_NX
);
