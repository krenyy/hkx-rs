use crate::hk_enum;

hk_enum!(
    u8,
    TriangleFormat,
    0 => TF_THREE_INT32S,
    1 => TF_THREE_INT16S,
    2 => TF_OTHER
);
