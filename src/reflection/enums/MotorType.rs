use crate::hk_enum;

hk_enum!(
    i8,
    MotorType,
    0 => TYPE_INVALID,
    1 => TYPE_POSITION,
    2 => TYPE_VELOCITY,
    3 => TYPE_SPRING_DAMPER,
    4 => TYPE_CALLBACK,
    5 => TYPE_MAX
);
