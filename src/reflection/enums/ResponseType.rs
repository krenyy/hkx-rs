use crate::hk_enum;

hk_enum!(
   i8,
   ResponseType,
   0 => RESPONSE_INVALID,
   1 => RESPONSE_SIMPLE_CONTACT,
   2 => RESPONSE_REPORTING,
   3 => RESPONSE_NONE,
   4 => RESPONSE_MAX_ID
);
