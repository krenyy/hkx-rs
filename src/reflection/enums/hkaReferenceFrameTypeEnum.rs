use crate::hk_enum;

hk_enum!(
    i8,
    hkaReferenceFrameTypeEnum,
    0 => REFERENCE_FRAME_UNKNOWN,
    1 => REFERENCE_FRAME_DEFAULT,
    2 => REFERENCE_FRAME_PARAMETRIC
);
