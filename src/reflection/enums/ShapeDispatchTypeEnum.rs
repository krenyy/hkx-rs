use crate::hk_enum;

hk_enum!(
    u8,
    ShapeDispatchTypeEnum,
    0 => CONVEX_IMPLICIT,
    1 => CONVEX,
    2 => HEIGHT_FIELD,
    3 => COMPOSITE,
    4 => USER,
    5 => NUM_DISPATCH_TYPES
);
