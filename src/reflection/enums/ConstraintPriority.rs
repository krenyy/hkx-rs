use crate::hk_enum;

hk_enum!(
    u8,
    ConstraintPriority,
    0 => PRIORITY_INVALID,
    1 => PRIORITY_PSI,
    2 => PRIORITY_SIMPLIFIED_TOI_UNUSED,
    3 => PRIORITY_TOI,
    4 => PRIORITY_TOI_HIGHER,
    5 => PRIORITY_TOI_FORCED,
    6 => NUM_PRIORITIES
);
