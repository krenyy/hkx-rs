use crate::hk_enum;

hk_enum!(
    u8,
    WeldingType,
    0 => WELDING_TYPE_ANTICLOCKWISE,
    4 => WELDING_TYPE_CLOCKWISE,
    5 => WELDING_TYPE_TWO_SIDED,
    6 => WELDING_TYPE_NONE
);
