pub mod AnimationType;
pub mod AtomType;
pub mod BlendHint;
pub mod BvTreeType;
pub mod CollectionType;
pub mod ConstraintPriority;
pub mod EdgeFlagBits;
pub mod MappingType;
pub mod MeasurementMode;
pub mod MotionType;
pub mod MotorType;
pub mod OnDestructionRemapInfo;
pub mod Platform;
pub mod ResponseType;
pub mod ShapeDispatchTypeEnum;
pub mod ShapeInfoCodecTypeEnum;
pub mod ShapeType;
pub mod SlotFlags;
pub mod SolvingMethod;
pub mod TriangleFormat;
pub mod VectorConversion;
pub mod WeldingType;
pub mod hkaReferenceFrameTypeEnum;

#[macro_export]
macro_rules! hk_enum {
    ($type:ty, $name:ident $(,$value:expr => $key:ident)+) => {
        #[derive(Clone, Debug, Default, PartialEq)]
        pub enum $name {
            #[default]
            $($key,)+
        }

        impl crate::binary::traits::HkxBinaryReadable for $name {
            fn binary_read(br: &mut crate::binary::reader::HkxBinaryReader) -> Self {
                match br.read::<$type>() {
                    $($value => Self::$key,)+
                    other => unreachable!("invalid enum value! ({})", other)
                }
            }
        }

        impl crate::binary::traits::HkxBinaryWritable for $name {
            fn binary_write(self, bw: &mut crate::binary::writer::HkxBinaryWriter, ser: &mut crate::serialize::SerializationData) {
                bw.write::<$type>(
                    ser,
                    match self {
                        $(Self::$key => $value,)+
                        other => unreachable!("invalid enum value! ({:?})", other)
                    }
                )
            }
        }
    };
}
