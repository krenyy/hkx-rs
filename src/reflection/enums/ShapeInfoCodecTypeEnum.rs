use crate::hk_enum;

hk_enum!(
    u8,
    ShapeInfoCodecTypeEnum,
    0 => NULL_CODEC,
    1 => UFM358,
    16 => MAX_NUM_CODECS
);
