use crate::hk_enum;

hk_enum!(
    u8,
    SlotFlags,
    0 => SF_NO_ALIGNED_START,
    1 => SF_16BYTE_ALIGNED_START,
    3 => SF_64BYTE_ALIGNED_START
);
