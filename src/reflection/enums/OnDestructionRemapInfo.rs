use crate::hk_enum;

hk_enum!(
    u8,
    OnDestructionRemapInfo,
    0 => ON_DESTRUCTION_REMAP,
    1 => ON_DESTRUCTION_REMOVE,
    2 => ON_DESTRUCTION_RESET_REMOVE
);
