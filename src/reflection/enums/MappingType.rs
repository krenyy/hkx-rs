use crate::hk_enum;

hk_enum!(
    i32,
    MappingType,
    0 => HK_RAGDOLL_MAPPING,
    1 => HK_RETARGETING_MAPPING
);
