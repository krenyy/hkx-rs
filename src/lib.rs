#![allow(non_camel_case_types, non_snake_case)]

mod binary;
mod deserialize;
mod packfile;
mod reflection;
mod serialize;

#[cfg(test)]
mod tests;

pub use deserialize::deserialize;
pub use packfile::header::variant::Variant;
pub use reflection::{classes, enums, HkObject};
pub use serialize::serialize;
